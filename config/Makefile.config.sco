# --------------------------------------------------------------------
# SMS Client, send messages to mobile phones and pagers
#
# Makefile.config
#
#  Copyright (C) 2014 portmaster - http://bsdforge.com
#  Copyright (C) 1997,1998,1999 Angelo Masci
#
#  All rights reserved.
#
# --------------------------------------------------------------------
# $Id$
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# SCO Open Server 5
# --------------------------------------------------------------------

PLATFORM = -DSCO
CC       = gcc
MAKE     = make
CFLAGS   = -g -I. -Wall -pedantic $(PLATFORM)
XTRALIBS = -lsocket

# --------------------------------------------------------------------

SMSUSER   = root
SMSGROUP  = root

# --------------------------------------------------------------------

PREFIX    =
BINPREFIX = $(PREFIX)
ETCPREFIX = $(PREFIX)
MANPREFIX = $(PREFIX)
SPOOLDIR  = /var/spool
LOGDIR    = /var/adm

MLOCALSMSRC   = .sms_addressbook
MGLOBALSMSRC  = $(ETCPREFIX)/etc/sms/sms_addressbook
MSERVICEDIR   = $(ETCPREFIX)/etc/sms
MLOGFILE      = $(LOGDIR)/smslog
MSNPPDLOGFILE = $(LOGDIR)/snppdlog
MSMSDLOGFILE  = $(LOGDIR)/smsdlog

# --------------------------------------------------------------------
# SMS_Client can be built to use the libmodem package or
# its own internal modem routines. Currently the internal routines
# are known to be unstable and are still considered to be in ALPHA
# Valid Values for MODEMLIB are:
#
#	$(LIBMODEM) - use the libmodem-1.0.0 packages with patches
# 	$(SMSMODEM) - use the internal modem routines

LIBMODEM = 1
SMSMODEM = 2

MODEMLIB = $(SMSMODEM)

# --------------------------------------------------------------------

BINDIR  = $(BINPREFIX)/bin
ETCDIR  = $(ETCPREFIX)/etc
MANDIR  = $(MANPREFIX)/man
MANEXT  = 1
INSTALL = /etc/install
RM      = /bin/rm -f
CP      = /bin/cp
TR	= /usr/bin/tr
AR	= /usr/local/bin/ar -rc
STRIP   = /usr/local/bin/strip
RANLIB  = echo ranlib

# --------------------------------------------------------------------

