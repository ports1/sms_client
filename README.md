      UNIX SMS Client 3.0.2 by (c) 2014-2021 portmaster_AT_bsdforge.com
      =================================================================

      Linux SMS Client 3.0.1 by (c) 1997,1998,1999,2000 Angelo Masci
      =================================================================


A simple UNIX client Allowing you to send SMS messages to mobile phones
and pagers. The software currently supports a number of providers
and protocols:
```
  +----------------------------------------------------------------------+
  | Service         Protocol      Notes                                  |
  +----------------------------------------------------------------------+
  | CELLNET         TAP           Supports multiple sends (see NOTE)     |
  | DETEMOBIL D1    TAP           Supports multiple sends (see NOTE)     |
  | D2              UCP                                                  |
  | EPLUS           TAP           Supports multiple sends (see NOTE)     |
  | AZCOM           TAP           Supports multiple sends (see NOTE)     |
  | CALLMAX         TAP           Supports multiple sends (see NOTE)     |
  | TELSTRA         TAP           Supports multiple sends (see NOTE)     |
  | VODAFONE        proprietary   UK/Australian Telenote services        |
  | VODAFONE_TAP    TAP           UK                                     |
  | ORANGE          proprietary   Supports multiple sends (see NOTE)     |
  |                               Includes Hutchinson Pagers             |
  | PAGEONE         proprietary   Supports multiple sends (see NOTE)     |
  | MINICALL        PAGEONE       Supports multiple sends (see NOTE)     |
  | ONE2ONE         proprietary   Supports multiple sends (see NOTE)     |
  | VODAPAGE        proprietary   Block Mode supported                   |
  | VODACOM         proprietary                                          |
  | PTT/KPN Telcom  proprietary   Supports multiple sends (see NOTE)     |
  | ANSWER          proprietary                                          |
  | MTN             proprietary                                          |
  | LIBERTEL        proprietary   Supports multiple sends (see NOTE)     |
  | TIM             proprietary   Supports multiple sends (see NOTE)     |
  | PROXIMUS        proprietary                                          |
  | AMPI            TAP                                                  |
  | EUROPOLITAN     CIMD          Currently in ALPHA and testing         |
  | BTEASYREACH     TAP           Currently in ALPHA and testing         |
  | SWISSCOM        UCP           Currently in ALPHA and testing         |
  | TELENOR         UCP           Currently in ALPHA and testing         |
  | HELLO                         Currently in ALPHA and testing         |
  | VOICESTREAM     TAP           Currently in ALPHA and testing         |
  | TELECOM NZ      TAP           Currently in ALPHA and testing         |
  | SKYTEL          TAP           Currently in ALPHA and testing         |
  | TELECOM NZ      TAP           Currently in ALPHA and testing         |
  | TELIA           UCP           Currently in ALPHA and testing         |
  | NETCOM          TAP           Currently in ALPHA and testing         |
  | MOBISTAR        UCP           Currently in ALPHA and testing         |
  | EIRPAGE         TAP           Currently in ALPHA and testing         |
  +----------------------------------------------------------------------+
  | CELLNET_WEB     proprietary   Currently in ALPHA and testing         |
  | ORANGE_WEB      proprietary   Currently in ALPHA and testing         |
  | PROXIMUS_WEB    proprietary   Currently in ALPHA and testing         |
  | ATT_WEB         proprietary   Currently in ALPHA and testing         |
  | NEXTEL_WEB      proprietary   Currently in ALPHA and testing         |
  | PAGENET_WEB     proprietary   Currently in ALPHA and testing         |
  | LYCOS_WEB       proprietary   Currently in ALPHA and testing         |
  +----------------------------------------------------------------------+
  |                 SNPP*         Currently in ALPHA and testing         |
  |                 GENERIC*      Currently in ALPHA and testing         |
  +----------------------------------------------------------------------+
```
Using an unlisted provider that allow TAP access should be quite straight
forward.

There are a large number of services that do not appear to use TAP but
instead simple user interfaces for interactive use by a user dialing up
with a modem. For several UK based services such as these I have written
drivers, note that providers often offer more that one service and as
such you may require a different driver for each one.

**NOTE** -
```
      Services supporting 'multiple sends' allow the same message to
       be sent to several recipients with only a single connection
       having to be established, saving the cost of addition connection
       charges.

       It should be noted that ORANGE although allowing multiple
       sends does seem to limit them to a maximum of 3 messages per
       connection. Likewise, PTT/KPN Telcom has a limit of 2 messages
       per connection.
```

**NOTE** -
```
       Protocols marked '*' are using ALPHA version drivers.
       SNPP support is limited as we don't currently have any SNPP
       servers to connect to other than 'snppd'. The GENERIC driver
       uses a script to connect to either modem based or tcp/ip based
       servers, it's still early days for this driver but is
       interesting to play with.
```
SMS Client 2.0.7 can use the optional libmodem package written by
Riccardo Facchetti. Prior to version 2.0.7 this package was a requirement
to build and use the SMS Client. You can obtain it from sunsite.unc.edu in
/pub/Linux/libs/ as libmodem-1.3.tar.gz

**NOTE** -
```
       Prior to version 1.3 of libmodem a patch was required, this is no
       longer the case. To use libmodem instead of the builtin modem
       handling routines edit 'Makefile.config'
```

**To build the sms_client binary on Linux/Solaris:**
```
        sh configure
	make ; make install
```
**NOTE** -
```
       To build the sms_client binary under on a different Operating
       System copy the appropriate 'Makefile.config.OS' file from the
       config directory along with 'Makefile', rename the
       'Makefile.config.OS' to 'Makefile.config' edit if necessary
       and run 'make ; make install'
```
**Example using the SMS Client:**
```
        sms-client [SERVICE:]XXXXXXXX "Test Message 1" "Test Message 2" ...
                       |         |         |
                       |         |         +-- The message you
                       |         |             want to send
                       |         |             Maximum 150 Characters
                       |         |
                       |         +------------ Mobile or Pager ID
                       |
                       |                       For mobiles phones this is
                       |                       usually the telephone number,
                       |                       In some cases this must be
                       |                       written in international format.
                       |                       ie. For UK Numbers remove leading
                       |                       0 and add 44 prefix
                       |
                       +---------------------- Optional SERVICE name
```
You can use a simple address book file called sms_addressbook
This is your global resource file, it should contain the name of
your default service and possibly commonly used numbers. See the
sms_addressbook file for examples. Any user can also create a local
version of this file which should be placed in their home directory as
.sms_addressbook The names are searched for in the user's local resource
file and and finally the global resource file, this is so a user can
override global names.

The sms/sms_services file contains a list of services and the protocols
that should be used when sending a message via that service.

Each service must have a corresponding SERVICE file in sms/services,
this file instructs the software which settings to use for the specified
service, the service centre number to dial and additional comms parameters.
A number of these service files exist and can be found in the sms/services
directory.

**Configuring Drivers:**

You may want of modify the number of services that are supported
by sms_client, you can do this simply by editing Makefile.config
and adding or removing drivers from the DRIVERS line. You must then
rebuild and install the sms_client binary. To obtain a list of drivers
currently built into your binary simply type:
```
	sms_client -d
```
**Writing Drivers:**

This is much more straight forward than you may think. Take a look
at src/driver/skeleton.c for an example driver.

**Return Values:**

On error sms_client WILL return some useful error codes so that you can
determine what went wrong. For each message sent you receive output
in the form:
```
	[ERROR] SERVICE:NUMBER "MESSAGE"
```
The ERROR is set to 000 for successful delivery any positive value
indicates there was a problem in delivery. See sms_error.h for description
of values.

If all messages were delivered successfully then sms_client returns 0
any other value indcates one or more delivery problems occurred.

**Contributions:**

A New 'contrib' directory has been added which contains some useful
scripts, at the moment we have:
```
	mail2sms  (Andy Hawkins)    - perl script for forwarding E-Mail via sms.
	www       (David Usherwood) - WWW Frontend
```
**NOTE** -
```
       These scripts were written for sms_client-2.0.5 and may not work
       correctly with this version as I have not tested them.
```
**Credits:**

I would like to express my thanks to the following individuals for
their help in testing, hardware, debugging, support and general
feedback:
```
	Mike Casella		<mike@lines0.uwic.ac.uk>
	Guy William Hayton 	<guy@hayton.demon.co.uk>
	David Hill		<dave@minnie.demon.co.uk>
	Chris Voce		<cvoce@rcsuk.com>
	Frans Andersson 	<frans@concept.se>
	Jonathan M. Hunter	<jon@ninja.ml.org>
	Geoff Peacock		<geoff@ccmobile.com>
	Harald Milz		<hm@seneca.muc.de>
	Job J. van Gorkum	<JvGorkum@gbnetworks.com>
	Nick Andrew		<nick@gidora.zeta.org.au>
	Jon Laughton 		<jon@eoin.demon.co.uk>
	Michael Josephson 	<michael@josephson.org>
	Dimitri Brukakis 	<dimitri.brukakis@omp.de>
	Are Tysland 		<arety@dolphinics.no>
	Petter Reinholdtsen	<pere@td.org.uit.no>
	Chris Berrington	<chrisb@redac.co.uk>
	Tim Ruehsen		<TRuehsen@aol.com>
	Gareth Abel 		<abelge@behp72.gpt.co.uk>
	Jeff Duffy		<jduffy@semcor.com>
	Sergio Barresi 		<sbarresi@imispa.it>
	matth 			<matth@labyrinth.net.au>
	David Ockwell-Jenner 	<doj@nortelnetworks.com>
	Chuck Hurd 		<hurd@hurd.is.ge.com>
	Jonas Borgstrom 	<jonas_b@bitsmart.com>
	Fredrik Bjork		<Fredrik.Bjork.List@varbergenergi.se>
	Alexander Grapenthin 	<a.grapenthin@sicomtec.de>
```
**Contrib Credits:**

Forward queries concerning 'mail2sms' perl script
to the author:
```
		Andy Hawkins	<andy@gently.demon.co.uk>
```
Forward queries concerning 'mail2sms' shell script
to the author:
```
		Matt Foster 	<matt@molnir.demon.co.uk>
```
Forward queries concerning 'smsweb'
to the author:
```
		David Usherwood <David_Usherwood@infocat.co.uk>
```
**Driver Credits:**

Forward queries concerning Australian Telenote service
using the VODAFONE driver to the patch contributor:
```
		Jeremy Laidman		<JLaidman@AUUG.org.au>
```
Forward queries concerning VODACOM and MTN
to the author:
```
		Alf Stockton 	<stockton@fast.co.za>
```
Forward queries concerning VODAPAGE 'verbose'
to the author:
```
		Neil A. Hillard	<hillardn@gkn-whl.co.uk>
```
Forward queries concerning LIBERTEL
to the authors:
```
		Henk Wevers 	<jhwevers@telekabel.nl>
		Aart Koelewijn 	<aart@mtack.xs4all.nl>
```
Forward queries concerning TIM
to the authors:
```
		Massimo Nuvoli  <massimo@nuvoli.to.it>
				<adrieder@sbox.tu-graz.ac.at>
```
Forward queries concerning PROXIMUS
to the author:
```
		Mario Brackeva 	<mariob@jeeves.be>
```
Forward queries concerning AZCOM
to the contributor:
```
		Brad Smith	<brad@figint.com>
```
Forward queries concerning TELSTRA
to the contributor:
```
		Paul Gampe 	<paulg@apnic.net>
```
Forward queries concerning PTT
to the contributor:
```
		Harold Baur	<bo@bikerider.com>
```
Forward queries concerning ANSWER
to the contributor:
```
		Paul Andrew 	<PaulJ@Optimation.co.nz>
```
Forward queries concerning VSTREAM
to the contributor:
```
		doughnut	<doughnut@doughnut.net>
```
Forward queries concerning NZ
to the contributor:
```
		Robbie Poharama <robbiep@sequent.com>
```
Forward queries concerning SKYTEL
to the contributor:
```
		Kal Kolberg	<Kal.Kolberg@BassHotels.com>
```
Forward queries concerning FREEBSD
to the contributor:
```
		Nick Hibma 	<nick.hibma@jrc.it>
```
Forward queries concerning NETCOM
to the contributor:
```
		Bjorn Rogeberg 	<bjornrg@a.sol.no>
```
Forward queries concerning RPM Packages
to the contributor:
```
		Ross Golder <rossigee@bigfoot.com>
```
Forward queries concerning Debian Packages
to the contributor:
```
		Michael Holzt <kju@flummi.de>
```
Forward queries concerning MOBISTAR
to the contributor:
```
			      <yves.seynaeve@eyc.be>
```
Contact information:
This Software is still considered BETA release. If you have any
comments, problems, patches and improvements, please contact the
author:
```
		Chris Hutchinson <portmaster_AT_bsdforge.com>
		Angelo Masci	<angelo@styx.demon.co.uk>
```
WWW Sites:
```
	https://gitlab.com/ports1/sms_client
	https://BSDforge.com/projects/comms/sms_client
	http://www.styx.demon.co.uk/
```

Mailing List:
'Mark Lewis' has kindly set up a mailing list for SMS Client.
If you would like to join, send and email to 'majordomo@medusa.myth.co.uk'
with the following text in the body of the message:
```
	subscribe sms_client
```
Mark can be contacted via email at mark@mythic.net
