Summary:     SMS text messaging applications
Name:        sms_client
Version:     2.0.8u
Release:     1
Source:      sms_client-2.0.8u.tar.gz
Patch:       sms_client-fhs.patch
URL:         http://www.styx.demon.co.uk/
Copyright:   Open
Group:       Application/Communications
BuildRoot:   /tmp/%{name}-%{version}-root
Packager:    Ross Golder <rossigee@bigfoot.com>

%description
A simple command-line system for sending SMS messages to pagers and mobiles.

%prep
%setup
%patch

%build
./configure
make

%install
install -d $RPM_BUILD_ROOT/etc/sms
install	-d $RPM_BUILD_ROOT/etc/sms/services
install	-d $RPM_BUILD_ROOT/etc/sms/scripts
install -d $RPM_BUILD_ROOT/usr/bin
install -m 755 bin/* $RPM_BUILD_ROOT/usr/bin
install -d $RPM_BUILD_ROOT/usr/man/man1
install -m 644 docs/sms_client.1 $RPM_BUILD_ROOT/usr/man/man1
install -d $RPM_BUILD_ROOT/var/spool/sms
install -d $RPM_BUILD_ROOT/var/spool/sms/errors
install -d $RPM_BUILD_ROOT/var/spool/sms/locks
install -d $RPM_BUILD_ROOT/var/spool/sms/incoming
install -d $RPM_BUILD_ROOT/var/spool/sms/named_pipes
install -d $RPM_BUILD_ROOT/var/spool/sms/services
install -d $RPM_BUILD_ROOT/var/spool/sms/scripts
( cd sms/services ; \
    for service in * ; do \
	install -m 644 $service $RPM_BUILD_ROOT/etc/sms/services; \
	install -d $RPM_BUILD_ROOT/var/spool/sms/services/$service ; \
	install -d $RPM_BUILD_ROOT/var/spool/sms/errors/$service ; \
    done )
install -m 644 sms/scripts/email $RPM_BUILD_ROOT/etc/sms/scripts
install -m 644 sms/sms_config $RPM_BUILD_ROOT/etc/sms
install -m 644 sms/sms_addressbook $RPM_BUILD_ROOT/etc/sms
install -m 644 sms/sms_modem $RPM_BUILD_ROOT/etc/sms
install -m 644 sms/sms_services $RPM_BUILD_ROOT/etc/sms
install -m 644 sms/sms_daemons $RPM_BUILD_ROOT/etc/sms
install -d $RPM_BUILD_ROOT/var/log/sms

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README README.1ST FAQ Changelog docs/sms_protocol
%config /etc/sms/*
/usr/bin/*
/usr/man/man1/*
/var/spool/sms/*
/var/log/sms
