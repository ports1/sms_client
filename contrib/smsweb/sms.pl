#!/usr/bin/perl
use CGI qw(:standard);

$query = new CGI;
print $query->header,
	start_html("Web Pager Gateway"),
	h1("Web Pager Gateway"),
	hr();
unless ($query->param) {
&getnums;
print $query->start_form( -name=>"Input");
print "Enter your message here:";
print $query->hr;
print $query->textarea( -name=>'message',
			-rows=>2,
			-columns=>80);
	
print $query->hr("Select the recipients (use the Control key to pick several)");
print $query->p;
print $query->scrolling_list(
	-name=>'recipients',
	-values =>\@vals,
	-size=>8,
	-multiple=>'true'
	)
	;
print $query->hr("Press this button to send");
print $query->p;
print $query->submit(-value=>'Send Message');	 		
print $query->end_html;
}
else {
@names = $query->param;
$message = $query->param('message');
@recipients = $query->param('recipients');
$recipientlist = join(",",@recipients);
$recipientlist =~ s/ //go;
$output = '/usr/bin/sms_client ' . 
  $recipientlist . 
  " \"" . 
  $message . 
  "\" >&smsout";
system $output;
print "Message \"$message\" sent to: " ;
print $recipientlist;
print p;
#print $output;
print $query->end_html;
}
sub getnums {
open SMSRC, '/etc/smsrc';
while (<SMSRC>) {
	next if /^#/;
	next if /^SMS_default_service/;
	($name,$num) = split("=");
	chop $num;
	$code{$name} = $num if $num;
	}
@vals = keys %code;
}
