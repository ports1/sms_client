/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* server_lib.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <pwd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <netdb.h>

/* -------------------------------------------------------------------- */

#define MAX_BACKLOG 5

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int server_init(int server_port)
{
        int 	sockfd;  	/* listen on sock_fd		*/
	struct 	sockaddr_in 
		server_addr;    /* my address information 	*/


	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{	fprintf(stderr, "socket\n");
		exit(1);
	}

	server_addr.sin_family      = AF_INET; 			/* host byte order 		*/
	server_addr.sin_port        = htons(server_port); 	/* short, network byte order 	*/
	server_addr.sin_addr.s_addr = INADDR_ANY; 		/* auto-fill with my IP 	*/

	memset(&(server_addr.sin_zero), 0, 8);			/* zero rest of struct 		*/

	if (bind(sockfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) < 0)
	{
		switch (errno)
		{
		case EADDRINUSE:
			fprintf(stderr, "bind - Socket %d allready in use\n", server_port);
			break;
		case EACCES:
			fprintf(stderr, "bind - Socket %d protected port and not superuser\n", server_port);
			break;
		default:
			fprintf(stderr, "bind\n");
		}

		exit(1);
	}

	if (listen(sockfd, MAX_BACKLOG) < 0)
	{	fprintf(stderr, "listen\n");
		exit(1);
	}

	fprintf(stderr, "Listening on port %d\n", server_port);
	return sockfd;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int server_accept(int sockfd)
{

	int 	sin_size,
		new_sockfd;

	struct 	sockaddr_in 
		client_addr;


	sin_size   = sizeof(struct sockaddr_in);
	new_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &sin_size);

	fprintf(stderr, "Accepted connection.\n");

	return new_sockfd;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int server_connect(int port)
{
	int 	sockfd,
		new_sockfd;


	sockfd     = server_init(port);
	new_sockfd = server_accept(sockfd);

	return new_sockfd;
}
