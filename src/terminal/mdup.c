#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>


#include "terminal_io.h"
#include "logfile_io.h"

#include "logfile/logfile.h"
#include "common/common.h"
#include "comms/comms.h"
#include "error.h"


#define LOGFILE "logfile"


int main(int argc, char **argv)
{
	char 	*number,
		*comms_params;

	long	baud;
	int 	fdin,
		fdout;

#if 0
static  struct termios
        t;
#endif

	/* ---------------------------- */

	set_logfile("./logfile");
	set_loglevel(3);
	set_consolelog(TRUE);

	/* ---------------------------- */

	if (argc != 4)
	{
		lprintf(LOG_ERROR, "Usage: mdup <number> <params> <baud>\n");
		exit(1);
	}

	number       = argv[1];
	comms_params = argv[2];
	baud         = atoi(argv[3]);

	/* ---------------------------- */


	lprintf(LOG_STANDARD, "Dialing SMSC %s...\n", number);

	fdin = SMS_dial(number, comms_params, baud);
	if (fdin < 0)
	{       lprintf(LOG_WARNING, "Failed to connect\n");
		return EDIAL;
	}

	lprintf(LOG_STANDARD, "Connection Established.\n");


	fdout = fdin;

	init_logfile("logs/terminal.log");

	setterm();

#if 0
        if (tcgetattr(fdin, &t) == -1)
        {       lprintf(LOG_WARNING, "MODEM: Failed tcgetattr()\n");
		exit(1);
	}

        t.c_cc[VMIN]  = (unsigned short)256;   /* 4096 Character buffer                */
        t.c_cc[VTIME] = 0;      /* Block indefinitely                   */

        if (tcsetattr(fdin, TCSANOW, &t) == -1)
        {       lprintf(LOG_WARNING, "MODEM: Failed tcsetattr()\n");
		exit(1);
	}
#endif

	terminal_io(STDIN_FILENO, STDOUT_FILENO, fdin, fdout, -1);

	SMS_hangup(fdin);
	return 0;
}
