/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* snoop.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "terminal_io.h"
#include "logfile_io.h"
#include "server_lib.h"
#include "client_lib.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	int 	client_fdin,
		client_fdout,
		server_fdin,
		server_fdout,
		sockfd;


	/* Usage: snoop <local_port> <server_address> <server_port> */

	if (argc != 4)
	{
		fprintf(stderr, "Usage: snoop <local_port> <server_address> <server_port>\n");
		exit(-1);
	}


	sockfd = server_init(atoi(argv[1]));


	init_logfile("logs/snoop.log");

	setterm();
	while(1)
	{
		/* Loop for each client connection reconnect 	*/
		/* to the server 				*/

		client_fdin  = server_accept(sockfd);
		client_fdout = client_fdin;

		server_fdin  = client_connect(argv[2], atoi(argv[3]));
		server_fdout = server_fdin;


#if !defined(INTEL_NT)
		terminal_io(client_fdin, client_fdout, server_fdin, server_fdout, STDOUT_FILENO);
#else
		/* Select() only works on sockets so we can't use 	*/
		/* it to multiplex output to the terminal.		*/

		terminal_io(client_fdin, client_fdout, server_fdin, server_fdout, -1);
#endif

#if !defined(INTEL_NT)
		close(client_fdin);
		close(server_fdin);
#else
		closesocket(client_fdin);
		closesocket(server_fdin);		
#endif
	}

	return 0;
}
