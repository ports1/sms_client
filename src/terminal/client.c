/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* client.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "terminal_io.h"
#include "logfile_io.h"
#include "client_lib.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	int 	fdin,
		fdout;


	/* Usage: client <host> <port> */


	fdin = client_connect(argv[1], atoi(argv[2]));
	fdout = fdin;


	init_logfile("logs/client.log");

	setterm();
	terminal_io(STDIN_FILENO, STDOUT_FILENO, fdin, fdout, -1);
	return 0;
}

