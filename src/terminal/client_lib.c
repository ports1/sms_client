/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* client_lib.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include "client_lib.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int client_connect(char *host, int port)
{
	int sockfd;
	struct hostent *he;
	struct sockaddr_in their_addr; 		/* connector's address information */


	if ((he=gethostbyname(host)) == NULL) 
	{	perror("Client gethostbyname");
	  	exit(1);		/* get the host info */
	}

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{	perror("Client socket");
		exit(1);
	}

	their_addr.sin_family = AF_INET;	/* host byte order 		*/
	their_addr.sin_port = htons(port); 	/* short, network byte order 	*/

	their_addr.sin_addr = *((struct in_addr *)he->h_addr);

	memset(&(their_addr.sin_zero[0]), '\0', 8);

	if (connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) 
	{	perror("Client connect");
		exit(1);
	}


	return sockfd;
}

