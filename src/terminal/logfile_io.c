/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* logfile_io.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <assert.h>
#include <termios.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <netdb.h>

#include "logfile_io.h"
#include "common/common.h"

/* -------------------------------------------------------------------- */

#define INIT_SEND   0
#define CLIENT_SEND 1
#define SERVER_SEND 2


static int flog = -1;
static int sender_state = INIT_SEND;
static int new_paragraph = 0;

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void init_logfile(char *logfile)
{
	flog = open(logfile, O_WRONLY|O_TRUNC|O_CREAT, S_IRWXU|S_IRGRP|S_IROTH);
	if (flog == -1)
	{
		fprintf(stderr, "Error: opening file '%s'\n", logfile);
		exit(-1);
	}

	new_paragraph = 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void log_client_send(void)
{
	if (flog != -1)
	{
		if (sender_state != CLIENT_SEND)
		{	
			/* Just switched */

			if ((sender_state != INIT_SEND) &&
			    (!new_paragraph))
			{
				write(flog, "\"\n", strlen("\"\n"));
			}

			write(flog, "Client --> \"", strlen("Client --> \""));
			new_paragraph = 0;
		}

		sender_state = CLIENT_SEND;
	}
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void log_server_send(void)
{
	if (flog != -1)
	{
		if (sender_state != SERVER_SEND)
		{	
			/* Just switched */

			if ((sender_state != INIT_SEND) &&
			    (!new_paragraph))
			{
				write(flog, "\"\n", strlen("\"\n"));
			}

			write(flog, "Server --> \"", strlen("Server --> \""));
			new_paragraph = 0;
		}

		sender_state = SERVER_SEND;
	}
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void log_character(char c)
{
static	char buf[16];


	if (flog != -1)
	{
		if (new_paragraph)
		{
			write(flog, "           \"", strlen("           \""));
			new_paragraph = 0;
		}

		if (c == '\n') 
		{

			write(flog, "\\n\"\n", strlen("\\n\"\n"));	/* Ouput data to logfile */
			new_paragraph = 1;
		}
		else
		if (c == '\r')
		{
			write(flog, "\\r", strlen("\\r"));	/* Ouput data to logfile */
		}
		else
		if ((int)c < 32)
		{
			sms_snprintf(buf, 16, "\\0%02X", (int)c);

			write(flog, buf, strlen(buf));		/* Ouput data to logfile */
		}
		else
		{
			write(flog, &c, 1);	/* Ouput data to logfile */
		}
	}
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void close_logfile(void)
{
	if (flog != -1)
	{
		close(flog);
	}
}
