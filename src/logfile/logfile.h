/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* logfile/logfile.h								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#define LOG_OFF		0
#define LOG_ERROR	1
#define LOG_WARNING	2
#define LOG_STANDARD	3
#define LOG_VERBOSE	4
#define LOG_VERYVERBOSE	5

#define MAX_LOG_LINE	4096

/* -------------------------------------------------------------------- */

void open_log(void);
void close_log(void);
void set_logfile(char *logfile);
void set_loglevel(int loglevel);
void set_consolelog(int send_to_console_log);

#if !defined(__GNUC__)
void lprintf(int loglevel, const char *fmt, ...);
#else
void lprintf(int loglevel, const char *fmt, ...)
             __attribute__ ((format (printf, 2, 3)));
#endif

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

