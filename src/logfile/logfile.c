/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* logfile.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>

#include "logfile/logfile.h"
#include "error.h"
#include "common/common.h"

/* -------------------------------------------------------------------- */

char *ctable[] = {

	"<NUL>", "<SOH>", "<STX>", "<ETX>", "<EOT>",
	"<ENQ>", "<ACK>", "<BEL>", "<BS>",  "<HT>",
	"<LF>",  "<VT>",  "<NP>",  "<CR>",  "<SO>",
	"<SI>",  "<DLE>", "<DC1>", "<DC2>", "<DC3>",
	"<DC4>", "<NAK>", "<SYN>", "<ETB>", "<CAN>",
	"<EM>",  "<SUB>", "<ESC>", "<FS>",  "<GS>",
	"<RS>",  "<US>"
};

#define asctostr(X) (ctable[(int)(X)])

/* -------------------------------------------------------------------- */

static int console_log       = TRUE;
static int current_loglevel  = LOG_STANDARD;
static char *current_logfile = NULL;
static FILE *log_fp;

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void set_logfile(char *logfile)
{	current_logfile = logfile;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void set_loglevel(int loglevel)
{	current_loglevel = loglevel;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void set_consolelog(int send_to_console_log)
{	console_log = send_to_console_log;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void open_log(void)
{
	if (current_logfile == NULL)
	{	fprintf(stderr, "ERROR: logfile undefined\n");
		exit(EOPENLOG);
	}

	log_fp = fopen(current_logfile, "a");
	if (log_fp == NULL)
	{
		fprintf(stderr, "ERROR: Opening logfile: %s\n", current_logfile);
		fflush(stderr);	

		exit(EOPENLOG);
	}
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void close_log(void)
{
	if (log_fp != NULL)
	{	fclose(log_fp);
	}
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *get_current_logtype(int loglevel)
{
	switch (loglevel)
	{
	case LOG_ERROR:
		return "ERROR";
	case LOG_WARNING:
		return "WARNING";
	}

	return "";
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *get_current_date(void) 
{
static	char	buf[128];
	time_t	ct;

	struct tm
		*ctm;

	
	time(&ct);
	ctm = localtime(&ct);
	strftime(buf, 64, "%b %d %H:%M:%S", ctm);

	return buf;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void lprintf(int loglevel, const char *fmt, ...)
{
	va_list args;

static	char	line[MAX_LOG_LINE],
		nline[MAX_LOG_LINE * 5];	/* 5 is the maximum	*/
						/* width of results 	*/
						/* from asctostr()	*/
		
	char	*ptr;
	int	i, 
		line_len;
	

	/* ---------------------------- */

	if (loglevel > current_loglevel)
		return;


	open_log();


	va_start(args, fmt);
#if !defined(LINUX)
	vsprintf(line, fmt, args);
#else
	vsnprintf(line, MAX_LOG_LINE, fmt, args);
#endif
	va_end(args);


	line_len = strlen(line);
	ptr = nline;
	for (i=0; i<line_len; i++)
	{
		if ((line[i] <= 31) &&
		    (line[i] >= 0))
		{
			if ((line[i] == '\n') &&
			    (i == (line_len -1)))
			{
				*ptr = '\n';
				ptr++;
			}
			else
			{	strcpy(ptr, asctostr(line[i]));
				ptr += strlen(asctostr(line[i]));
			}
		}
		else
		if (line[i] < 0)
		{
			*ptr = ' ';
			ptr++;
		}
		else
		{	*ptr = line[i];
			ptr++;
		}
	}

	*ptr = '\0';


	if (console_log)
	{
		if (loglevel > LOG_WARNING)
		{
			fprintf(stdout, "%s", nline);
			fflush(stdout);	
		}	
		else
		{	fprintf(stderr, "%s: %s",
				get_current_logtype(loglevel),
				nline);

			fflush(stderr);	
		}
	}
		
	fprintf(log_fp, "%s [%d] %s: %s",
		get_current_date(),
	        (int)getpid(),
		get_current_logtype(loglevel),
		nline);
		
	fflush(log_fp);

	close_log();
}
