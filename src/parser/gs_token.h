/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_token.h								*/
/*									*/
/*  Copyright (C) 1997,1998 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#if !defined(_TOKEN_H_)
#define _TOKEN_H_ 1

#include <stdio.h>

/* -------------------------------------------------------------------- */
/* Maximum number of tokens we can store in a				*/
/* heap.								*/
/* -------------------------------------------------------------------- */

#define MAX_TOKENS	1024

/* -------------------------------------------------------------------- */

struct token_struct
	{
		char 	*str;
		long 	value;		/* Maybe we want numbers */
		int	type;
		int	ptr_type;
		void	*ptr;
	};

typedef struct token_struct TOKEN;

/* -------------------------------------------------------------------- */

struct token_list_struct
	{
		TOKEN *token;
		struct token_list_struct *next;
	};

typedef struct token_list_struct TOKEN_LIST;

/* -------------------------------------------------------------------- */

struct token_heap_struct
	{
		TOKEN_LIST
			*list;
	};

typedef struct token_heap_struct TOKEN_HEAP;

/* -------------------------------------------------------------------- */

typedef TOKEN * TOKEN_ID;

extern 	TOKEN_ID	
	null_tok,
	eof_tok,
	assignment_tok,
	lparen_tok,
	rparen_tok,
	lcurly_tok,
	rcurly_tok,
	comma_tok,
	dot_tok,
	semicolon_tok;

/* -------------------------------------------------------------------- */

#define T_NULL		0
#define T_IDENTIFIER	1
#define T_SPECIAL	2
#define T_STRING	3
#define T_NUMERIC	4

#define TP_NULL		0
#define TP_STRING	1
#define TP_DICTIONARY	2
#define TP_LIST		3
#define TP_NUMERIC	4

/* -------------------------------------------------------------------- */

TOKEN_ID get_next_token(int fd, TOKEN_HEAP *heap);
TOKEN_ID add_token(TOKEN_HEAP *heap, char *str, int type, int ptr_type, void *ptr);

char *get_token_strvalue(TOKEN_HEAP *heap, TOKEN_ID token_id);
long get_token_numvalue(TOKEN_HEAP *heap, TOKEN_ID token_id);
char *get_token_strtype(TOKEN_HEAP *heap, TOKEN_ID token_id);
char *get_token_indirect_strvalue(TOKEN_HEAP *heap, TOKEN_ID token_id);

int init_builtin_heap(void);
void push_back_character(int character);
void push_back_token(TOKEN_ID token);
void dump_heap(TOKEN_HEAP *heap, char *name);
void dump_heaptofile(TOKEN_HEAP *heap, char *name, FILE *fp);
void token_assign_strvalue(TOKEN_HEAP *heap, TOKEN_ID token_id, char *string);
void token_assign_numvalue(TOKEN_HEAP *heap, TOKEN_ID token_id, long value);
void token_assign_dictionary(TOKEN_HEAP *heap, TOKEN_ID token_id, TOKEN_HEAP *dictionary);
void token_assign_list(TOKEN_HEAP *heap, TOKEN_ID token_id, TOKEN_HEAP *dictionary);

int get_token_type(TOKEN_HEAP *heap, TOKEN_ID token_id);
int get_token_indirect_type(TOKEN_HEAP *heap, TOKEN_ID token_id);

TOKEN_HEAP *generate_new_heap(void);
TOKEN_HEAP *get_token_indirect_dictionary(TOKEN_HEAP *heap, TOKEN_ID token_id);
TOKEN_HEAP *get_token_indirect_list(TOKEN_HEAP *heap, TOKEN_ID token_id);
TOKEN_HEAP *dup_heap(TOKEN_HEAP *heap);

TOKEN_HEAP *init_heap(TOKEN_HEAP *heap);
void free_heap(TOKEN_HEAP *heap);


/* -------------------------------------------------------------------- */


TOKEN_LIST *gs_add_token_list_item(TOKEN_LIST *list, TOKEN **token, char *str, int type, int ptr_type, void *ptr);
TOKEN *gs_new_token(char *str, int type, int ptr_type, void *ptr);
TOKEN *gs_find_item(TOKEN_LIST *list, char *str, int type);

/* -------------------------------------------------------------------- */
#endif
