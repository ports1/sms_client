/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_getvar.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>

#include "gs_token.h"
#include "gs_parser.h"
#include "gs_translate.h"

#include "gs_api.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN *gs_get_variable(TOKEN *dictionary, char *string)
{
	TOKEN_HEAP
		*heap;


	if (! gs_isdictionary(dictionary))
	{
		return NULL;
	}

	heap = get_token_indirect_dictionary(NULL, dictionary);
	if (heap == NULL)
	{	return NULL;
	}

	return get_var(heap, string);
}


/* -------------------------------------------------------------------- */
/* Return Values:							*/
/*									*/
/*	The string value assigned to this identifier.			*/
/* 	NULL on error							*/
/*									*/
/* -------------------------------------------------------------------- */
char *gs_get_strvalue(TOKEN *token)
{
	if (! gs_isidentifier(token))
	{
		return NULL;
	}

	return token->ptr;
}


/* -------------------------------------------------------------------- */
/* Return Values:							*/
/*									*/
/*	The label/name of this 'Identfier', 'Dictionary' or 'List'	*/
/*	NULL on error							*/
/* -------------------------------------------------------------------- */
char *gs_get_labelvalue(TOKEN *token)
{
	return token->str;
}

