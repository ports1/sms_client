/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* api_test.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>

#include "common/common.h"
#include "logfile/logfile.h"

#include "gs_token.h"
#include "gs_list.h"
#include "gs_translate.h"
#include "gs_parser.h"
#include "gs_io.h"

#include "gs_api.h"

/* -------------------------------------------------------------------- */

#define DICT_LOAD "myfile.load"
#define DICT_SAVE "myfile.save"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char **argv)
{

	TOKEN_LIST
		*eptr;

	TOKEN 	
		*item,
		*token;


	set_logfile(LOGFILE);
	set_loglevel(LOGLEVEL);
	set_consolelog(TRUE);


	/* ---------------------------------------- */

	item = create_dictionary("dictionary");

	if (gs_isdictionary(item))
	{	lprintf(LOG_STANDARD, "Item is Dictionary\n");
	}
	else
	{	lprintf(LOG_STANDARD, "Item is NOT Dictionary\n");
		exit(1);
	}


	/* ---------------------------------------- */

	if (gs_loaddictionaryfromfile(DICT_LOAD, item) == -1)
	{	lprintf(LOG_ERROR, "Failed to load Dictionary '%s'\n", DICT_LOAD);
		exit(1);
	}
	else
	{	lprintf(LOG_STANDARD, "Loaded Dictionary '%s'\n", DICT_LOAD);
	}


	/* ---------------------------------------- */


	eptr = NULL;
	while ((token = gs_get_dictionary_next(item, &eptr)) != NULL) 
	{
		if (gs_isidentifier(token))
		{
			lprintf(LOG_STANDARD, "Identifier '%s'\n", gs_get_labelvalue(token));
		}
		else
		if (gs_isdictionary(token))
		{
			lprintf(LOG_STANDARD, "Dictionary '%s'\n", gs_get_labelvalue(token));
		}
		else
		if (gs_islist(token))
		{
			lprintf(LOG_STANDARD, "List '%s' len '%d'\n", gs_get_labelvalue(token), gs_get_list_length(token));

		}
		else
		{
			lprintf(LOG_STANDARD, "Unknown\n");
		}

		if (eptr == NULL)
		{	break;
		}
	}

	if (eptr != NULL)
	{
		lprintf(LOG_ERROR, "Something went wrong!\n");
	}


	/* ---------------------------------------- */


	if (gs_savedictionarytofile(DICT_SAVE, item) == -1)
	{	lprintf(LOG_ERROR, "Failed to Save Dictionary '%s'\n", DICT_SAVE);
		exit(1);
	}
	else
	{	lprintf(LOG_STANDARD, "Saved Dictionary '%s'\n", DICT_SAVE);
	}

	return 0;
}

