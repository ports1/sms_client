/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_type.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>

#include "common/common.h"
#include "gs_api.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int gs_isidentifier(TOKEN *item)
{
	if (item != NULL)
	{
		if (item->ptr_type == TP_STRING)
		{	return TRUE;
		}
	}

	return FALSE;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int gs_isdictionary(TOKEN *item)
{
	if (item != NULL)
	{
		if (item->ptr_type == TP_DICTIONARY)
		{	return TRUE;
		}
	}

	return FALSE;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int gs_islist(TOKEN *item)
{
	if (item != NULL)
	{
		if (item->ptr_type == TP_LIST)
		{	return TRUE;
		}
	}

	return FALSE;
}


/* -------------------------------------------------------------------- */
/* Return Values:							*/
/*									*/
/*	TRUE  if Item is a simple data item				*/
/* 	FALSE if Item is an 'Identifier', 'Dictionary' or 'List'	*/
/*									*/
/* -------------------------------------------------------------------- */
int gs_isdata(TOKEN *item)
{
	return (!(gs_isidentifier(item) || gs_isdictionary(item) || gs_islist(item)));
}
