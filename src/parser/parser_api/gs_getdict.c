/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_getdict.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include "common/common.h"

#include "gs_token.h"
#include "gs_translate.h"
#include "gs_parser.h"

#include "gs_api.h"

/* -------------------------------------------------------------------- */
/*									*/
/* Return Values:							*/
/*									*/
/* 	Token of type 'Identifier', 'Dictionary' or 'List' or		*/
/* 	NULL if an error ocuured.					*/
/*									*/
/*	*ptr is set to NULL when we are at the end of the list.		*/
/*									*/
/* -------------------------------------------------------------------- */
TOKEN *gs_get_dictionary_next(TOKEN *item, TOKEN_LIST **ptr)
{
	TOKEN_HEAP 
		*heap;
	TOKEN_LIST	
		*list;
	TOKEN	*token;



	if (! gs_isdictionary(item))
	{
		return NULL;
	}

	if (*ptr == NULL)
	{
		heap = (TOKEN_HEAP *)(item->ptr);
		if (heap == NULL)
		{
			return NULL;
		}

		list = (TOKEN_LIST *)(heap->list);
		if (list == NULL)
		{
			return NULL;
		}
	}
	else
	{	list = *ptr;
	}


	while ((list != NULL) && (gs_isdata(list->token)))
	{
		/* Skip ALL items which are simply	*/
		/* Data. We only want to return		*/
		/* 'Identifiers', 'Dictionaries' or 	*/
		/* 'Lists'				*/

		list = list->next;
	}

	token = list->token;
	if (token == NULL)
	{
		return NULL;
	}
	
	*ptr = list->next;
	return token;
}


