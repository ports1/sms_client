/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_load.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>

#include "gs_parser.h"

#include "gs_api.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int gs_loaddictionaryfromfile(char *filename, TOKEN *dictionary)
{
	int	fd;

	TOKEN_HEAP 
		*heap;

	TOKEN	*token_id;


	if (! gs_isdictionary(dictionary))
	{	return -1;
	}


	if (init_builtin_heap() == -1)
	{	return -1;
	}


	heap = get_token_indirect_dictionary(NULL, dictionary);
	if (heap == NULL)
	{	return -1;
	}

	fd = gs_open(filename);
	if (fd == -1)
	{	return -1;
	}

	if (parse_dictionary(fd, heap))
	{
		token_id = get_next_token(fd, heap);
		if (token_id != eof_tok)
		{
			gs_close(fd);
			return -1;
		}
	}
	else
	{	gs_close(fd);
		return -1;
	}
	
	if (gs_close(fd))
	{	return -1;
	}

	return 0;
}

