/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_getlist.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include "common/common.h"

#include "gs_token.h"
#include "gs_translate.h"
#include "gs_parser.h"

#include "gs_api.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN *gs_get_list_element(TOKEN *item, int index)
{
	return NULL;
}

/* -------------------------------------------------------------------- */
/* Return Value:							*/
/*									*/
/*	The numbers of items in the list or				*/
/*	-1 to inidicate and error.					*/
/* -------------------------------------------------------------------- */
int gs_get_list_length(TOKEN *item)
{
	TOKEN	*token;
	TOKEN_LIST
		*eptr;

	int 	len;


	len = 0;


	eptr = NULL;
	while ((token = gs_get_list_next(item, &eptr)) != NULL)
	{
		if (! gs_isdata(token))
		{
			len++;
		}


		if (eptr == NULL)
		{
			break;
		}
	}

	if (eptr != NULL)
	{
		return -1;
	}

	return len;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN *gs_get_list_next(TOKEN *item, TOKEN_LIST **ptr)
{
	TOKEN_HEAP 
		*heap;
	TOKEN_LIST	
		*list;
	TOKEN	*token;



	if (! gs_islist(item))
	{
		return NULL;
	}

	if (*ptr == NULL)
	{
		heap = (TOKEN_HEAP *)(item->ptr);
		if (heap == NULL)
		{
			return NULL;
		}

		list = (TOKEN_LIST *)(heap->list);
		if (list == NULL)
		{
			return NULL;
		}
	}
	else
	{	list = *ptr;
	}


	while ((list != NULL) && (gs_isdata(list->token)))
	{
		/* Skip ALL items which are simply	*/
		/* Data. We only want to return		*/
		/* 'Identifiers', 'Dictionaries' or 	*/
		/* 'Lists'				*/

		list = list->next;
	}

	token = list->token;
	if (token == NULL)
	{
		return NULL;
	}
	
	*ptr = list->next;
	return token;
}


