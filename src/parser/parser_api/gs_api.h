/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_api.h								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include "gs_token.h"
#include "gs_private.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

TOKEN *create_dictionary(char *name);

TOKEN *gs_get_dictionary_next(TOKEN *item, TOKEN_LIST **ptr);
TOKEN *gs_get_list_next(TOKEN *item, TOKEN_LIST **ptr);
TOKEN *gs_get_list_element(TOKEN *item, int index);
int gs_get_list_length(TOKEN *item);

TOKEN *gs_get_variable(TOKEN *dictionary, char *string);
char *gs_get_strvalue(TOKEN *token);
char *gs_get_labelvalue(TOKEN *token);

int gs_loaddictionaryfromfile(char *filename, TOKEN *dictionary);
int gs_savedictionarytofile(char *filename, TOKEN *dictionary);

int gs_isidentifier(TOKEN *item);
int gs_isdictionary(TOKEN *item);
int gs_islist(TOKEN *item);
int gs_isdata(TOKEN *item);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
