/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* test_parser.c							*/
/*									*/
/*  Copyright (C) 1997,1998 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>

#if defined(LINUX)
#include <getopt.h>
#endif

#include "common/common.h"
#include "logfile/logfile.h"
#include "gs_token.h"
#include "gs_translate.h"

/* -------------------------------------------------------------------- */

#if !defined(MLOGFILE)
#error "MLOGFILE undefined"
#else
#define LOGFILE         MLOGFILE
#endif

#if !defined(MLOGLEVEL)
#error "MLOGLEVEL undefined" 
#else
#define LOGLEVEL	MLOGLEVEL
#endif

/* -------------------------------------------------------------------- */

static TOKEN_HEAP	
		*global_heap;

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void usage(char *file)
{
	lprintf(LOG_ERROR, "Usage: %s [-l loglevel] <variable> <file>\n", file);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	int 	c;
	char	*ptr,
	 	*str;


	/* ---------------------------- */

	set_logfile(LOGFILE);
	set_loglevel(LOGLEVEL);
	set_consolelog(TRUE);

	/* ---------------------------- */

	while ((c = getopt (argc, argv, "l:")) != -1)
        {
                switch (c)
                {
                        case 'l':  
				set_loglevel((int)strtol(optarg, &ptr, 10));
				if (ptr == optarg)
				{
					lprintf(LOG_ERROR, "Option l requires an argument\n");
	     	                        usage(argv[0]);
                	                exit(-1);
				}
                                
                                break;

                        case '?':
                                lprintf(LOG_ERROR, "Unknown option `-%c'\n", optopt);
                                usage(argv[0]);
                                exit(-1);

                        default:
                                abort ();
                }
        }



	if ((argc - optind) != 2)
	{	usage(argv[0]);
		exit(-1);
	}

	/* ---------------------------- */

	global_heap = gs_parse_file(argv[optind+1]);
	if (global_heap == NULL)
	{
		lprintf(LOG_ERROR, "Parsing file '%s'\n", argv[optind+1]);
		exit(-1);
	}

	dump_heap(global_heap, "global");

	str = get_strvalue(global_heap, argv[optind]);
	if (str == NULL)
	{	printf("Var '%s' Not Found\n", argv[optind]);
	}
	else
	{	printf("Var '%s' Found\n", argv[optind]);
		printf("%s = %s\n", argv[optind], str);
	}


	free_heap(global_heap);
	return 0;
}

