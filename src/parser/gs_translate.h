/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_translate.h							*/
/*									*/
/*  Copyright (C) 1997,1998 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include "gs_token.h"

/* -------------------------------------------------------------------- */

TOKEN_HEAP *gs_parse_file(char *file);
TOKEN_HEAP *get_varlist(TOKEN_HEAP *heap, char *variable);
TOKEN_HEAP *dup_varlist(TOKEN_HEAP *heap, char *variable);
TOKEN_HEAP *get_varlist_element(TOKEN_HEAP *heap, int index);

char *get_strvalue_element(TOKEN_HEAP *heap, int index);
char *get_strvalue(TOKEN_HEAP *heap, char *variable);
char *in_varlist(TOKEN_HEAP *current_heap, char *variable);
TOKEN *get_var(TOKEN_HEAP *heap, char *variable);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

