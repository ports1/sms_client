/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* gs_translate.c							*/
/*									*/
/*  Copyright (C) 1997,1998 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>

#include "common/common.h"
#include "logfile/logfile.h"

#include "gs_token.h"
#include "gs_translate.h"
#include "gs_parser.h"
#include "error.h"

#include "gs_private.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static char *get_word(char *string, char **eptr);


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN_HEAP *gs_parse_file(char *file)
{
	int	fd;

	TOKEN_HEAP 
		*token_heap;
	TOKEN_ID
		token_id;


	if (init_builtin_heap() == -1)
	{	return NULL;
	}

	token_heap = (TOKEN_HEAP *)malloc(sizeof(TOKEN_HEAP));
	if (token_heap == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		return NULL;
	}

	token_heap->list = NULL;


	fd = gs_open(file);
	if (fd == -1)
	{	return NULL;
	}

	if (parse_dictionary(fd, token_heap))
	{
		token_id = get_next_token(fd, token_heap);
		if (token_id != eof_tok)
		{
			gs_close(fd);
			return NULL;
		}
	}
	else
	{	gs_close(fd);
		return NULL;
	}
	
	if (gs_close(fd))
	{	return NULL;
	}

	return token_heap;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *get_word(char *string, char **eptr)
{
static	char 	word[1024],
		*word_ptr,
		*ptr;


	if (*string == '\0')
	{	return NULL;
	}

	word_ptr = word;
	ptr = string;
	while (*ptr != '\0')
	{
		if (*ptr == '.')
		{	break;
		}

		*word_ptr++ = *ptr++;
	}

	*word_ptr = '\0';

	if (*ptr != '\0')
	{	ptr++;
	}

	*eptr = ptr;
	return word;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *get_strvalue(TOKEN_HEAP *heap, char *variable)
{
	char 	*ptr,
		*str,
		*word;

	TOKEN	*token;


	lprintf(LOG_VERYVERBOSE, "Searching for variable '%s'\n", variable);

	str = NULL;
	word = get_word(variable, &ptr);
	while(word != NULL)
	{
		token = gs_find_item(heap->list, word, T_IDENTIFIER);
		if (token == NULL)
		{	return NULL;
		}

		if (get_token_type(heap, token) == T_IDENTIFIER)
		{
			if ((get_token_indirect_type(heap, token) == TP_STRING) ||
			    (get_token_indirect_type(heap, token) == TP_NUMERIC))
			{
				str = get_token_indirect_strvalue(heap, token);
			}
			else
			if ((get_token_indirect_type(heap, token) == TP_DICTIONARY) ||
			    (get_token_indirect_type(heap, token) == TP_LIST))
			{
				heap = get_token_indirect_dictionary(heap, token);
			}
		}
		else
		{	return NULL;
		}

		word = get_word(ptr, &ptr);
	}


	return str;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN *get_var(TOKEN_HEAP *heap, char *variable)
{
	char 	*ptr,
		*word;

	TOKEN	*token;


	lprintf(LOG_VERYVERBOSE, "Searching for variable '%s'\n", variable);

	word = get_word(variable, &ptr);
	while(word != NULL)
	{
		token = gs_find_item(heap->list, word, T_IDENTIFIER);
		if (token == NULL)
		{	return NULL;
		}

		if (get_token_type(heap, token) == T_IDENTIFIER)
		{
			if ((get_token_indirect_type(heap, token) == TP_DICTIONARY) ||
			    (get_token_indirect_type(heap, token) == TP_LIST))
			{
				heap = get_token_indirect_dictionary(heap, token);
			}
		}
		else
		{	return NULL;
		}

		word = get_word(ptr, &ptr);
	}


	return token;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN_HEAP *get_varlist(TOKEN_HEAP *heap, char *variable)
{
	char 	*ptr,
		*word;

	TOKEN	*token;


	word = get_word(variable, &ptr);
	while(word != NULL)
	{
		token = gs_find_item(heap->list, word, T_IDENTIFIER);
		if (token == NULL)
		{	return NULL;
		}

		if (get_token_type(heap, token) == T_IDENTIFIER)
		{
			if ((get_token_indirect_type(heap, token) == TP_DICTIONARY) ||
			    (get_token_indirect_type(heap, token) == TP_LIST))
			{
				heap = get_token_indirect_dictionary(heap, token);
			}
			else
			{	return NULL;
			}
		}
		else
		{	return NULL;
		}

		word = get_word(ptr, &ptr);
	}

	return heap;
}



/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN_HEAP *get_varlist_element(TOKEN_HEAP *heap, int index)
{
	char 	var[64];


	sms_snprintf(var, 64, "[%d]", index);
	return get_varlist(heap, var);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *get_strvalue_element(TOKEN_HEAP *heap, int index)
{
	char 	var[64];


	sms_snprintf(var, 64, "[%d]", index);
	return get_strvalue(heap, var);
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
TOKEN_HEAP *dup_varlist(TOKEN_HEAP *heap, char *variable)
{
	char 	*ptr,
		*word;

	TOKEN	*token;


	word = get_word(variable, &ptr);
	while(word != NULL)
	{
		token = gs_find_item(heap->list, word, T_IDENTIFIER);
		if (token == NULL)
		{	return NULL;
		}

		if (get_token_type(heap, token) == T_IDENTIFIER)
		{
			if ((get_token_indirect_type(heap, token) == TP_DICTIONARY) ||
			    (get_token_indirect_type(heap, token) == TP_LIST))
			{
				heap = get_token_indirect_dictionary(heap, token);
			}
			else
			{	return NULL;
			}
		}
		else
		{	return NULL;
		}

		word = get_word(ptr, &ptr);
	}

	/* duplicate the heap 	*/
	/* and return it.	*/

	if (heap != NULL)
	{	heap = dup_heap(heap);
	}

	return heap;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *in_varlist(TOKEN_HEAP *heap, char *variable)
{
	char	var[64],
		*str;
	int	i;


	i = 0;
	while(TRUE)
	{
		sms_snprintf(var, 64, "[%d]", i);
		str = get_strvalue(heap, var);
		if (str != NULL)
		{	if (strcmp(str, variable) == 0)
			{	return str;
			}
		}
		else
		{	return NULL;
		}

		i++;
	}

	return NULL;
}
