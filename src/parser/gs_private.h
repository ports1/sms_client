/* -------------------------------------------------------------------- */
/* gs_private.h								*/
/*									*/
/*  Copyright (C) 2000 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

int gs_open(char *file);
int gs_close(int fd);

void gs_init_linenumber(void);
void gs_increment_linenumber(void);
void gs_decrement_linenumber(void);
int gs_get_linenumber(void);

int gs_get_next_character(int fd, unsigned char *character);
int gs_push_character(unsigned char character);
int gs_pop_character(unsigned char *character);
int gs_escape_character(int fd, unsigned char *escaped_character);

/* -------------------------------------------------------------------- */

