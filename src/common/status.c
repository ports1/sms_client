/* -------------------------------------------------------------------- */
/* 									*/
/*									*/
/*									*/
/* Copyright (C) 2000 Angelo Masci					*/
/*       								*/
/* This program is free software; you can redistribute it and/or modify	*/
/* it under the terms of the GNU General Public License as published by	*/
/* the Free Software Foundation; either version 2 of the License, or	*/
/* (at your option) any later version.					*/
/*									*/
/* This program is distributed in the hope that it will be useful,	*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	*/
/* GNU General Public License for more details.				*/
/*									*/
/* You should have received a copy of the GNU General Public License	*/
/* along with this program; if not, write to the Free Software		*/
/* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		*/
/* 									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "format.h"
#include "status.h"
#include "common.h"

/* -------------------------------------------------------------------- */

static STATUS emalloc =
{
	"malloc() failed\n",
	NULL
};

static STATUS estatus =
{
	"status_stack_message() failed\n",
	&emalloc
};

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static STATUS *status_stack_message(STATUS *status, char *msg);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static STATUS *status_stack_message(STATUS *status, char *msg)
{
	STATUS *item;

	item = (STATUS *)malloc(sizeof(STATUS));
	if (item == NULL)
	{	return &estatus;
	}

	item->msg  = msg;
	item->next = status;

	return item;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
STATUS *status_stack_errno(STATUS *status, int error)
{
	return status_stack(status, "%s\n", strerror(error));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
STATUS *status_stack(STATUS *status, char *fmt, ...)
{
	va_list args;
	char	*buf;
	

	va_start(args, fmt);
	libcommon_vasprintf(&buf, fmt, args);
	va_end(args);
	
	return status_stack_message(status, buf);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void status_output(STATUS *status, void (*status_output_message)(char *))
{
	STATUS *item;

	item = status;
	while (item != NULL)
	{
		(*status_output_message)(item->msg);
		item = item->next;
	}
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void status_free(STATUS *status)
{
	if (status->next != NULL)
	{	status_free(status->next);
	}

	free(status->msg);
	free(status);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void status_default_output(char *str)
{
	libcommon_fprintf(stderr, str);
}


