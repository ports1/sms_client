/* -------------------------------------------------------------------- */
/* 									*/
/*									*/
/*									*/
/* Copyright (C) 2000 Angelo Masci					*/
/*       								*/
/* This program is free software; you can redistribute it and/or modify	*/
/* it under the terms of the GNU General Public License as published by	*/
/* the Free Software Foundation; either version 2 of the License, or	*/
/* (at your option) any later version.					*/
/*									*/
/* This program is distributed in the hope that it will be useful,	*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	*/
/* GNU General Public License for more details.				*/
/*									*/
/* You should have received a copy of the GNU General Public License	*/
/* along with this program; if not, write to the Free Software		*/
/* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		*/
/* 									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#include <stdarg.h>
#include <stdlib.h>

#include "common.h"
#include "format.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
struct charbuf_struct
{
	char 	*buf,
		*ptr,
		**pbuf;
	int	len;
};

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int copytobuf(void *cfarg, char *str, int len)
{
	struct 	charbuf_struct 
		*cbuf;
	int	i;


	cbuf = (struct charbuf_struct *)cfarg;

	cbuf->len  += len;
	cbuf->buf   = realloc(cbuf->buf, cbuf->len);
	*cbuf->pbuf = cbuf->buf;
	cbuf->ptr   = cbuf->buf + (cbuf->len - len);

	i = len;
	while (i--)
	{	*cbuf->ptr++ = *str++;
	}

	*cbuf->ptr = '\0';
	return len;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int libcommon_vasprintf(char **buf, char *fmt, va_list args)
{
	struct charbuf_struct cbuf;

	cbuf.buf  = NULL;
	cbuf.ptr  = NULL;
	cbuf.pbuf = buf;
	cbuf.len  = 0;

	return format(copytobuf, NULL, &cbuf, fmt, args);
}

 

