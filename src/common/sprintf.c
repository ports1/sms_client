/* -------------------------------------------------------------------- */
/* 									*/
/*									*/
/*									*/
/* Copyright (C) 2000 Angelo Masci					*/
/*       								*/
/* This program is free software; you can redistribute it and/or modify	*/
/* it under the terms of the GNU General Public License as published by	*/
/* the Free Software Foundation; either version 2 of the License, or	*/
/* (at your option) any later version.					*/
/*									*/
/* This program is distributed in the hope that it will be useful,	*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	*/
/* GNU General Public License for more details.				*/
/*									*/
/* You should have received a copy of the GNU General Public License	*/
/* along with this program; if not, write to the Free Software		*/
/* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		*/
/* 									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#include <stdarg.h>

#include "common.h"
#include "format.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int libcommon_sprintf(char *buf, char *fmt, ...)
{
	va_list args;
	int 	res;

	va_start(args, fmt);
	res = libcommon_vsprintf(buf, fmt, args);
	va_end(args);

	return res;
} 

