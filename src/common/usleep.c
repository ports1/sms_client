/* -------------------------------------------------------------------- */
/* usleep.c								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>

#include "common.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void libcommon_usleep(long microseconds)
{
#if defined(LINUX)
	usleep(microseconds);
#else
        struct  timeval 
                timeout;
        
        timeout.tv_usec = microseconds % 1000000;
        timeout.tv_sec  = microseconds / 1000000;
        
        select(0, NULL, NULL, NULL, &timeout);
#endif
}
