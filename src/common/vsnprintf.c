/* -------------------------------------------------------------------- */
/* 									*/
/*									*/
/*									*/
/* Copyright (C) 2000 Angelo Masci					*/
/*       								*/
/* This program is free software; you can redistribute it and/or modify	*/
/* it under the terms of the GNU General Public License as published by	*/
/* the Free Software Foundation; either version 2 of the License, or	*/
/* (at your option) any later version.					*/
/*									*/
/* This program is distributed in the hope that it will be useful,	*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	*/
/* GNU General Public License for more details.				*/
/*									*/
/* You should have received a copy of the GNU General Public License	*/
/* along with this program; if not, write to the Free Software		*/
/* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		*/
/* 									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdarg.h>

#include "common.h"
#include "format.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
struct charbuf_struct
{
	char 	*buf,
		*ptr;
	int	len;
};

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int copytobuf(void *cfarg, char *str, int len)
{
	struct 	charbuf_struct 
		*cbuf;
	int	n;


	cbuf = (struct charbuf_struct *)cfarg;

	n = 0;
	while (len--)
	{
		if (cbuf->len == -1)
		{
			*cbuf->ptr++ = *str++;
			n++;
		}
		else
		if (cbuf->len == 0)
		{
			*cbuf->ptr = '\0';
		}
		else
		{	cbuf->len--;
			*cbuf->ptr++ = *str++;
			n++;
		}
	}

	return n;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int libcommon_vsnprintf(char *buf, int len, char *fmt, va_list args)
{
	struct charbuf_struct cbuf;

	cbuf.buf = buf;
	cbuf.ptr = buf;
	cbuf.len = len;

	return format(copytobuf, NULL, &cbuf, fmt, args);
}

 

