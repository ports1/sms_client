/* -------------------------------------------------------------------- */
/* common.h								*/
/*									*/
/*  Copyright (C) 1999 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#if !defined(_COMMON_H)
#define _COMMON_H 1

#include <stdarg.h>
#include <stdio.h>

/* -------------------------------------------------------------------- */

#if !defined(FALSE)
#define FALSE 		0
#endif
#if !defined(TRUE)
#define TRUE 		(!FALSE)
#endif

#if defined(NEXT)
typedef int     	ssize_t;
typedef int     	pid_t;
typedef unsigned short  mode_t;
typedef short   	nlink_t;
#endif

/* -------------------------------------------------------------------- */

#define MAXLINELEN 	256		/* Commonly used for buffers 	*/

/* -------------------------------------------------------------------- */

#define sms_snprintf libcommon_snprintf

/* -------------------------------------------------------------------- */

void libcommon_usleep(long microseconds);
char *libcommon_strfcat(char *path, char *element);
char *libcommon_getword(char *ptr, char **endptr);

int libcommon_vsnprintf(char *buf, int len, char *fmt, va_list args);
int libcommon_snprintf(char *buf, int len, char *fmt, ...);
int libcommon_vsprintf(char *buf, char *fmt, va_list args);
int libcommon_sprintf(char *buf, char *fmt, ...);

int libcommon_vfprintf(FILE *fp, char *fmt, va_list args);
int libcommon_fprintf(FILE *fp, char *fmt, ...);
int libcommon_vprintf(char *fmt, va_list args);
int libcommon_printf(char *fmt, ...);

int libcommon_vasprintf(char **buf, char *fmt, va_list args);
int libcommon_asprintf(char **buf, char *fmt, ...);

/* -------------------------------------------------------------------- */

#endif
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
