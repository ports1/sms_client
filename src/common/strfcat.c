/* -------------------------------------------------------------------- */
/* strfcat.c								*/
/*									*/
/*  Copyright (C) 1997,1998 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>

#include "common.h"

/* -------------------------------------------------------------------- */
/* Add a file/directory name to a pathname. If the path			*/
/* does not end with '/' append one before adding the other		*/
/* file/dir.								*/
/* -------------------------------------------------------------------- */
char *libcommon_strfcat(char *path, char *src)
{
	char 	*dst,
		*lptr;


	lptr = NULL;
	dst  = path;
	while (*dst != '\0')
	{
		lptr = dst;
		dst++;
	}

	if ((lptr != NULL) && (*lptr == '/'))
	{
		dst = lptr;
	}

	*dst++ = '/';


	if (*src == '/')
	{	src++;
	}

	while (*src != '\0')
	{
		*dst = *src;

		dst++;
		src++;
	}

	*dst = '\0';

	return path;
}
