/* -------------------------------------------------------------------- */
/* getword.c								*/
/*									*/
/*									*/
/* Copyright (C) 1997,1998 Angelo Masci					*/
/*       								*/
/* This program is free software; you can redistribute it and/or modify	*/
/* it under the terms of the GNU General Public License as published by	*/
/* the Free Software Foundation; either version 2 of the License, or	*/
/* (at your option) any later version.					*/
/*									*/
/* This program is distributed in the hope that it will be useful,	*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	*/
/* GNU General Public License for more details.				*/
/*									*/
/* You should have received a copy of the GNU General Public License	*/
/* along with this program; if not, write to the Free Software		*/
/* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		*/
/* 									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "common.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *libcommon_getword(char *ptr, char **endptr)
{
	char 	*buf, 
		*buf_ptr,
		*lptr;

	int	len;


	/* ---------------------------------------- */

	while ((isspace(*ptr)) && 
               (*ptr != '\0'))
	{
		ptr++;	
	}

	/* ---------------------------------------- */

	if (*ptr == '\0')
	{	return NULL;
	}

	/* ---------------------------------------- */

	lptr = ptr;

	len = 0;
	while ((! isspace(*ptr)) && 
	       (*ptr != '\0'))
	{
		len++;
		ptr++;
	}

	ptr = lptr;

	/* ---------------------------------------- */

	buf = (char *)malloc(sizeof(char) * (len + 1));
	if (buf == NULL)
	{	return NULL;
	}

	/* ---------------------------------------- */

	buf_ptr = buf;
	while ((!isspace(*ptr)) && 
	       (*ptr != '\0'))
	{
		*buf_ptr++ = *ptr++;
	}

	*buf_ptr = '\0';
	*endptr  = ptr;

	/* ---------------------------------------- */

	return buf;
}
