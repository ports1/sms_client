/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
#if !defined(_STATUS_H_)
#define _STATUS_H_ 1

#include <stdio.h>

/* -------------------------------------------------------------------- */

struct status_struct
{
	char *msg;
	struct status_struct *next;
};

typedef struct status_struct STATUS;

/* -------------------------------------------------------------------- */

#define STATUS_OK NULL

/* -------------------------------------------------------------------- */

STATUS *status_stack(STATUS *status, char *fmt, ...);
STATUS *status_stack_errno(STATUS *status, int error);
void status_output(STATUS *status, void (*status_output_message)(char *));
void status_free(STATUS *status);
void status_default_output(char *str);

#endif
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

