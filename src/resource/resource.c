/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* sms_resource.c							*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "error.h"
#include "logfile/logfile.h"
#include "resource.h"
#include "common/common.h"
#include "parser/gs_token.h"
#include "parser/gs_translate.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static void set_defaults(char *filename, RESOURCE *resource_list, TOKEN_HEAP *resource_heap);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void set_defaults(char *filename, RESOURCE *resource_list, TOKEN_HEAP *resource_heap)
{
	RESOURCE
		*resource;
	char	*str;


	resource = resource_list;
	while (resource->type != RESOURCE_NULL)
	{
		if (resource->name == NULL)
		{	continue;
		}

		str = get_strvalue(resource_heap, resource->name);
		if (str != NULL)
		{
			/* Found item, set the resource value to it */


			if (resource->type == RESOURCE_STRING)
			{
				resource->str_value = (char *)malloc(sizeof(char) * (strlen(str) +1));
				if (resource->str_value == NULL)
				{
					lprintf(LOG_ERROR, "malloc() failed\n");
					exit(EMALLOC);
				}

				strcpy(resource->str_value, str);

				if (resource->default_warning)
				{	lprintf(LOG_VERBOSE, "Parsing '%s' Found '%s', setting value to '%s'\n", 
						filename, 
						(resource->name != NULL)?(resource->name):("NULL"),
						resource->str_value);
				}
			}
			else if (resource->type == RESOURCE_NUMERIC)
			{
				resource->int_value = atoi(str);

				if (resource->default_warning)
				{	lprintf(LOG_VERBOSE, "Parsing '%s' Found '%s', setting value to '%ld'\n", 
						filename, 
						(resource->name != NULL)?(resource->name):("NULL"),
						resource->int_value);
				}
			}			
		}
		else
		{
			/* Didn't Find item, set the resource value to it's default */


			if (resource->type == RESOURCE_STRING)
			{
				resource->str_value = resource->default_str_value;

				if (resource->default_warning)
				{	lprintf(LOG_VERBOSE, "Parsing '%s' Could not find '%s', using default '%s'\n", 
						filename, 
						(resource->name != NULL)?(resource->name):("NULL"), 
						(resource->str_value != NULL)?(resource->str_value):("NULL"));
				}
			}
			else if (resource->type == RESOURCE_NUMERIC)
			{
				resource->int_value = resource->default_int_value;

				if (resource->default_warning)
				{	lprintf(LOG_VERBOSE, "Parsing '%s' Could not find '%s', using default '%ld'\n", 
						filename, 
						(resource->name != NULL)?(resource->name):("NULL"),
						resource->int_value);
				}
			}			
		}


		/* Set the variable pointed to by the resource		*/
		/* to the value of the resource.			*/


		if (resource->type == RESOURCE_STRING)
		{
			if (resource->var != NULL)
			{	*((char **)(resource->var)) = resource->str_value;
			}

			lprintf(LOG_VERBOSE, "%s %s = %s\n", 
				filename, 
				(resource->name != NULL)?(resource->name):("NULL"), 
				(resource->str_value != NULL)?(resource->str_value):("NULL"));
		}
		else if (resource->type == RESOURCE_NUMERIC)
		{
			if (resource->var != NULL)
			{	*((long *)(resource->var)) = resource->int_value;
			}

			lprintf(LOG_VERBOSE, "%s %s = %ld\n", 
				filename, 
				(resource->name != NULL)?(resource->name):("NULL"), 
				resource->int_value);
		}
		else if (resource->type == RESOURCE_HEAP)
		{
			*((TOKEN_HEAP **)(resource->var)) = dup_varlist(resource_heap, resource->name);

			lprintf(LOG_VERBOSE, "%s %s = dup_heap()\n",
				filename, 
				(resource->name != NULL)?(resource->name):("NULL"));
		}			

		resource++;
	}
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int read_resource_file(char *filename, RESOURCE *resource_list, int warnings)
{
	TOKEN_HEAP	
		*resource_heap;


	lprintf(LOG_VERBOSE, "Parsing Resource File '%s'\n", filename);

	resource_heap = gs_parse_file(filename);
	if (resource_heap == NULL)
	{
		lprintf(LOG_ERROR, "Parsing file '%s'\n", filename);
		return RESOURCE_FILE_ERROR;
	}

	set_defaults(filename, resource_list, resource_heap);

	free_heap(resource_heap);
	return RESOURCE_FILE_OK;
}

/* -------------------------------------------------------------------- */
/* Differs from read_resource_file() in that it returns the		*/
/* Token Heap which must be freed by calling free_heap()		*/
/* -------------------------------------------------------------------- */
TOKEN_HEAP *parse_resource_file(char *filename, RESOURCE *resource_list, int warnings)
{
	TOKEN_HEAP	
		*resource_heap;


	lprintf(LOG_VERBOSE, "Parsing Resource File '%s'\n", filename);

	resource_heap = gs_parse_file(filename);
	if (resource_heap == NULL)
	{
		lprintf(LOG_ERROR, "Failed to parse file '%s'\n", filename);
		return NULL;
	}

	set_defaults(filename, resource_list, resource_heap);

	return resource_heap;
}


