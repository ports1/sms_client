/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* test_resource.c							*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "logfile/logfile.h"
#include "common/common.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

#if !defined(MLOGFILE)
#error "MLOGFILE undefined"
#else
#define LOGFILE         MLOGFILE
#endif

#if !defined(MLOGLEVEL)
#error "MLOGLEVEL undefined" 
#else
#define LOGLEVEL	MLOGLEVEL
#endif

/* -------------------------------------------------------------------- */

long	number_1,
	number_2;

char	*string_1,
	*string_2;

TOKEN_HEAP
	*heap = NULL;

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_NUMERIC,  "number_1", 	0, 1, NULL, 0,  NULL,    	   66,  	&number_1	},
		{ RESOURCE_NUMERIC,  "number_2", 	0, 1, NULL, 0,  NULL,    	   77,  	&number_2	},
		{ RESOURCE_STRING,   "string_1", 	0, 1, NULL, 0,  "string_1",        0,  		&string_1	},
		{ RESOURCE_STRING,   "string_2",	0, 1, NULL, 0,  "string_2",        0,  		&string_2	},
		{ RESOURCE_HEAP,     "list",		0, 1, NULL, 0,  NULL,        	   0,  		&heap	},
		{ RESOURCE_NULL,     NULL, 		0, 1, NULL, 0,  NULL,              0,  		NULL  	}
	};

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void usage(char *file)
{
	lprintf(LOG_ERROR, "Usage: %s [-l loglevel] <file>\n", file);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	int 	c;
	char	*ptr;


	/* ---------------------------- */

	set_logfile(LOGFILE);
	set_loglevel(LOGLEVEL);
	set_consolelog(TRUE);

	/* ---------------------------- */


	while ((c = getopt (argc, argv, "l:")) != -1)
        {
                switch (c)
                {
                        case 'l':  
				set_loglevel((int)strtol(optarg, &ptr, 10));
				if (ptr == optarg)
				{
					lprintf(LOG_ERROR, "Option l requires an argument\n");
	     	                        usage(argv[0]);
                	                exit(-1);
				}
                                
                                break;

                        case '?':
                                lprintf(LOG_ERROR, "Unknown option `-%c'\n", optopt);
                                usage(argv[0]);
                                exit(-1);

                        default:
                                abort ();
                }
        }

	if ((argc - optind) != 1)
	{	usage(argv[0]);
		exit(-1);
	}

	/* ---------------------------- */

	if (read_resource_file(argv[optind], resource_list, 1) == RESOURCE_FILE_ERROR)
	{	lprintf(LOG_ERROR, "Failed to read resource file '%s'\n", argv[optind]);
		exit(-1);
	}

	printf("number_1 = %ld\n", number_1);
	printf("number_2 = %ld\n", number_2);

	printf("string_1 = %s\n", string_1);
	printf("string_2 = %s\n", string_2);

	if (heap != NULL)
	{	dump_heap(heap, "list");
	}

	return 0;
}

