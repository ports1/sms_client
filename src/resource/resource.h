/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* sms_resource.h							*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#if !defined(_RESOURCE_H)
#define _RESOURCE_H 1

#include "parser/gs_token.h"

/* -------------------------------------------------------------------- */

#define RESOURCE_NULL		0
#define RESOURCE_STRING		1
#define RESOURCE_NUMERIC	2
#define RESOURCE_HEAP		3

/* -------------------------------------------------------------------- */

#define RESOURCE_FILE_OK	0
#define RESOURCE_FILE_ERROR	1

/* -------------------------------------------------------------------- */

struct resource_struct 
{
	int	type;
	char 	*name;
	
	int	found,
		default_warning;

	char 	*str_value;
	long	int_value;
	char 	*default_str_value;
	long	default_int_value;

	void	*var;
};

typedef struct resource_struct RESOURCE;

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

int read_resource_file(char *filename, RESOURCE *resource_list, int warnings);
TOKEN_HEAP *parse_resource_file(char *filename, RESOURCE *resource_list, int warnings);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

#endif
