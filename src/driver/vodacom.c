/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* vodacom.c								*/
/*									*/
/*  Copyright (C) 1998,1999 Alf Stockton             			*/ 
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id: vodacom.c,v 5.1 1998/02/01 07:10:39 root Exp root $
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "followed by RETURN, (or RETURN to quit)"; 
static char ACK2[] = "Please type your message, (maximum 160 characters), followed by RETURN.";

/* -------------------------------------------------------------------- */

static struct vodacom_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "7E1",     0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,      0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,      1200, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,      45, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,      15, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,      10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       0,    &(driver_env.def.max_deliver)  	 },
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,      0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int VODACOM_sendmessage(char *msisdn, char *message);
static int VODACOM_send_disconnect(void);
static void VODACOM_hangup(void);
	
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void VODACOM_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int VODACOM_sendmessage(char *msisdn, char *message)
{
	char 	buf[MAX_RESPONSE_BUFSIZE];
	char 	nmessage[1024];


	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Request\n");
		VODACOM_hangup();
		return EVODACOM_NORESPONSE;
	}


	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
   
	if (expstr(FD, buf, msisdn, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Returned\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Returned\n");
		VODACOM_hangup();
		return EVODACOM_NONUMBER;
	}

	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Request\n");
		VODACOM_hangup();
		return EVODACOM_NONUMBER;
	}

	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	sms_snprintf(nmessage, 1024, "\r\n\r\n>%s", message);

	if (expstr(FD, buf, nmessage, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
        {
	        lprintf(LOG_STANDARD, "Received Message back\n");
        }
	else
        {       lprintf(LOG_STANDARD, "No Message Returned\n");
	        VODACOM_hangup();
	        return EVODACOM_NOMESSAGE;
        }
    
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Request\n");
		VODACOM_hangup();
		return EVODACOM_NOMESSAGE;
	}

	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int VODACOM_send_disconnect(void)
{
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY vodacom_device = {

	"VODACOM",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	VODACOM_send_disconnect,
	default_single_deliver,
	VODACOM_sendmessage,
	default_login
};



