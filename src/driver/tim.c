/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* tim.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999,2000 Angelo Masci			*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/*  the author of this module						*/
/*  massimo@nuvoli.to.it   						*/
/*  adapted by 								*/
/*  adrieder@sbox.tu-graz.ac.at						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

/* Modified by Shevek <shevek@anarres.org> 2000-03-03                   *
 * TIM change their protocol                                            *
 * The system is essentially a dialup into a DUNIX box running Lynx,    *
 * this driver must manipulate the lynx browser.                        */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "ScripTIM";			/* S: This word is OK */
static char ACK2[] = "le frecce del terminale";		/* S: Bottom of form */
static char ACK3[] = "Non attivo";			/* S: Top of form */
static char ACK6[] = "Terminali abilitati";		/* S: Bottom of first */
static char ACK7[] = " per scollegarsi";
static char ACK8[] = "centri TIM";
static char ACKB[] = "MESSAGGIO INVIATO";		/* S: Delivery OK */
static char ACKC[] = "closed by foreign";		/* S: Hangup */

static char ARR_UP[] = "\033[A";	/* Up arrow for VT */
static char ARR_DN[] = "\033[B";	/* Down arrow for VT */
static char ARR_LT[] = "\033[D";	/* Left arrow for VT */

/* -------------------------------------------------------------------- */

static struct tim_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",  0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,   0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,   9600, 	 &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,   30, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,   20, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,   10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,   0,    	 &(driver_env.def.max_deliver)  	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,   0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int TIM_login(void);
static int TIM_sendmessage(char *msisdn, char *message);
static int TIM_send_disconnect(void);
static void TIM_hangup(void);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int TIM_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	/* S: Recognise the top of the first web page */
	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Tim Service Login\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Tim Service Login\n");
		
		TIM_hangup();
		return ETIM_NORESPONSE;
	}

	/* Recognise the lower half of the first web page (terminal) */
        if (expstr(FD, buf, ACK6, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
        {
                lprintf(LOG_STANDARD, "Tim Service Login\n");
        }
        else
        {
                lprintf(LOG_STANDARD, "No Tim Service Response\n");

                TIM_hangup();
                return ETIM_NORESPONSE;
        }

	/* S: <enter> on link to next page. */
        twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);	/* S */

	/* S: Recognise the prompt on the bottom of the second page */
        if (expstr(FD, buf, ACK7, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
        {
                lprintf(LOG_STANDARD, "Tim Service Terminal\n");
        }
        else
        {
                lprintf(LOG_STANDARD, "No Tim Service Response\n");

                TIM_hangup();
                return ETIM_NORESPONSE;
        }

	/* S: <enter> on link to next page. */
        twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	/* S: Recognise the third web page with the phone number */
        if (expstr(FD, buf, ACK8, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
        {
                lprintf(LOG_STANDARD, "Tim Service Menu\n");
        }
        else
        {
                lprintf(LOG_STANDARD, "No Tim Service Menu\n");

                TIM_hangup();
                return ETIM_NORESPONSE;
        }

	/* S: Accept the third web page */
        twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	/* S: We should now be looking at a form we must fill in. */
        if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
        {
                lprintf(LOG_STANDARD, "Tim Service Terminal\n");
        }
        else
        {
                lprintf(LOG_STANDARD, "No Tim Data Form (0)\n");

                TIM_hangup();
                return ETIM_NORESPONSE;
        }

        if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
        {
                lprintf(LOG_STANDARD, "Tim Menu Request\n");
        }
        else
        {
                lprintf(LOG_STANDARD, "No Tim Data Form (1)\n");

                TIM_hangup();
                return ETIM_NORESPONSE;
        }

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int TIM_sendmessage(char *msisdn, char *message)
{
	char buf[MAX_RESPONSE_BUFSIZE];

	/* S: Enter number into first form box */
	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	/* S: Skip four 'delay' fields, leaving them zero */
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	/* S: Enter message into text box */
	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	/* S: Press 'Invio' button */
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	if (expstr(FD, buf, ACKB, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		TIM_hangup();
		return ETIM_NODELIVERY;
	}

	/* S: To get back, we should go 'left down down' */
	twrite(FD, ARR_LT, strlen(ARR_LT), WRITETIMEOUT);

	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Tim Data Form (2)\n");
		
		TIM_hangup();
		return ETIM_NODELIVERY;
	}
	
	/* S: Get to the right button on the form (cycle) */
	twrite(FD, ARR_DN, strlen(ARR_DN), WRITETIMEOUT);
	twrite(FD, ARR_DN, strlen(ARR_DN), WRITETIMEOUT);

	/* S: XXX There isn't much we can do here to check that we
	 * are in the right place. Sending multiple messages with
	 * TIM is therefore not guaranteed. */

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int TIM_send_disconnect(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];

	/* S: Get onto a button rather than a field. */
	twrite(FD, ARR_UP, strlen(ARR_DN), WRITETIMEOUT);

	/* S: Quit lynx - 'Q' is quick quit. */
	twrite(FD, "Q", strlen("Q"), WRITETIMEOUT);

	if (expstr(FD, buf, ACKC, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Disconnection Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Disconnection Response\n");
		
		TIM_hangup();
		return ETIM_NODISCONNECT;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void TIM_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY tim_device = {

	"TIM",
	"1.2",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	TIM_send_disconnect,
	default_multiple_counted_deliver,
	TIM_sendmessage,
	TIM_login
};

