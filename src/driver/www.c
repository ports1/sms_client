/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* www.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static struct www_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

	char 	*url;

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",      0, 	  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       9600, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       3,    &(driver_env.def.max_deliver)  	},


		{ RESOURCE_STRING,  "SMS_url", 		0, 1, NULL, 0,  "http://localhost:80/", 0,  &(driver_env.url)		},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0,    NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int WWW_sendmessage(char *msisdn, char *message);
static void WWW_wrapper_hangup(DRIVER_DEFAULT_ENV *env);
static void WWW_hangup(void);
static int WWW_dial(DRIVER_DEFAULT_ENV *env);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int WWW_dial(DRIVER_DEFAULT_ENV *env)
{
	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int WWW_sendmessage(char *msisdn, char *message)
{
	char *ptr;
	int data_len;

ptr = get_document(driver_env.url, &data_len, "GET", NULL, NULL, NULL);
if (ptr == NULL)
{	return -1;
}

	/* Enter stateless code here */


	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void WWW_hangup(void)
{	
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void WWW_wrapper_hangup(DRIVER_DEFAULT_ENV *env)
{	WWW_hangup();
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY www_device = {

	"WWW",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_always_true,
	WWW_dial,
	WWW_wrapper_hangup,
	default_send_disconnect,
	default_single_deliver,
	WWW_sendmessage,
	default_login
};



