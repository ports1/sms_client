/* -------------------------------------------------------------------- */ 
/* SMS Client, send messages to mobile phones and pagers		*/ 
/*									*/ 
/* mtn.c 								*/
/*									*/ 
/*  Copyright (C) 1998,1999 Alf Stockton				*/ 
/*									*/ 
/*  This library is free software; you can redistribute it and/or	*/ 
/*  modify it under the terms of the GNU Library General Public 	*/ 
/*  License as published by the Free Software Foundation; either	*/ 
/*  version 2 of the License, or (at your option) any later version.	*/ 
/*									*/ 
/*  This library is distributed in the hope that it will be useful,	*/ 
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/ 
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/ 
/*  Library General Public License for more details.			*/ 
/*									*/ 
/*  You should have received a copy of the GNU Library General Public	*/ 
/*  License along with this library; if not, write to the Free		*/ 
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/ 
/*									*/ 
/*  You can contact the author at this e-mail address:			*/ 
/*									*/ 
/*  stockton@fast.co.za 						*/ 
/*									*/ 
/* -------------------------------------------------------------------- */ 
/* $Id: mtn.c,v 5.1 1998/02/01 07:10:39 root Exp root $ 
   -------------------------------------------------------------------- */ 
 
#include <stdio.h> 
#include <string.h> 
#include <unistd.h>
 
#include "common/common.h"
#include "logfile/logfile.h" 
#include "driver.h" 
#include "error.h" 
#include "comms/comms.h" 
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "Number : 083";
static char ACK3[] = "Please enter message :";
static char ACK4[] = "Wait for confirmation ? (Y/N)";

/* -------------------------------------------------------------------- */

static struct mtn_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",     0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,      0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,      1200, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,      60, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,      15, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,      10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       0,    &(driver_env.def.max_deliver)  	 },
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */ 
 
static void MTN_hangup(void); 
static int MTN_sendmessage(char *msisdn, char *message); 
 
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void MTN_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int MTN_sendmessage(char *msisdn, char *message)
{
	char 	buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "MTN Service Login\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No MTN Service Response\n");
		MTN_hangup();
		return EMTN_NORESPONSE;
	}

	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\n", strlen("\n"), WRITETIMEOUT); 

	if (expstr(FD, buf, msisdn, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Number Returned\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Returned\n");
		MTN_hangup();
		return EMTN_NONUMBER;
	}

	if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Message Request Received \n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Request\n");
		MTN_hangup();
		return EMTN_NONUMBER;
	}

	sleep(2);

	twrite(FD, message, strlen(message), WRITETIMEOUT); 
	twrite(FD, "\n", strlen("\n"), WRITETIMEOUT); 

	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Wait for confirmation Received \n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Wait Confirmation Response\n");
		MTN_hangup();
		return EMTN_NODELIVERY;
	}
   
	twrite(FD, "n\n", strlen("n\n"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "EXIT to exit Received \n");
	}
	else
	{	lprintf(LOG_STANDARD, "No EXIT to exit Response\n");
		MTN_hangup();
		return EMTN_NODELIVERY; 
	}

	twrite(FD, "exit\n", strlen("exit\n"), WRITETIMEOUT); 
 
	return 0; 
} 
 
/* -------------------------------------------------------------------- */ 
/* -------------------------------------------------------------------- */ 
DEVICE_ENTRY mtn_device = {

	"MTN",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	default_send_disconnect,
	default_single_deliver,
	MTN_sendmessage,
	default_login
};
