/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* orange.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id: orange.c,v 5.1 1998/02/01 07:10:39 root Exp root $
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "Welcome to Orange Dial-up Messaging\r\n";
static char ACK2[] = "Please choose an option (do not press return) : \r\n"
		     "\r\n"	
		     "       S   :- Send a message\r\n"
		     "       H   :- Help\r\n"
		     "       E   :- Exit\r\n";

static char ACK3[] = "Enter destination Orange or Hutchison Pager number and press return\r\n"
		     "\r\n"
		     ":";

static char ACK4[] = "Type your message (160 characters max) and press return to send\r\n"
		     "\r\n"
		     ":";

static char ACK5[] = "Message accepted - thankyou\r\n"
		     "\r\n";

static char ACK6[] = "Thanks for calling.\r\n"
		     "\r\n"
		     "The future's bright. The future's Orange.";

/* -------------------------------------------------------------------- */

static struct orange_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",      0, 	  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       9600, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       3,    &(driver_env.def.max_deliver)  	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0,    NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int ORANGE_login(void);
static int ORANGE_sendmessage(char *msisdn, char *message);
static int ORANGE_send_disconnect(void);
static void ORANGE_hangup(void);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int ORANGE_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Orange Service Login\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Orange Service Login\n");
		
		ORANGE_hangup();
		return EORANGE_NORESPONSE;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int ORANGE_sendmessage(char *msisdn, char *message)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Orange Service Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Orange Service Request\n");
		
		ORANGE_hangup();
		return EORANGE_NOSERVICE;
	}


	twrite(FD, "S", strlen("S"), WRITETIMEOUT);


	if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Request\n");
		
		ORANGE_hangup();
		return EORANGE_NONUMBER;
	}


	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);


	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Request\n");
		
		ORANGE_hangup();
		return EORANGE_NOMESSAGE;
	}


	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK5, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		ORANGE_hangup();
		return EORANGE_NODELIVERY;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int ORANGE_send_disconnect(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	twrite(FD, "E", strlen("E"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK6, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Disconnection Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Disconnection Response\n");
		
		ORANGE_hangup();
		return EORANGE_NODISCONNECT;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void ORANGE_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY orange_device = {

	"ORANGE",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	ORANGE_send_disconnect,
	default_multiple_counted_deliver,
	ORANGE_sendmessage,
	ORANGE_login
};



