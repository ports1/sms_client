/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* cimd.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999,2000 Angelo Masci			*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*  mattias								*/
/*  shevek@anarres.org 							*/
/*									*/
/*      Much of the work for this driver has been contributed		*/
/*	by shevek.							*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "comms/comms.h"
#include "error.h"
#include "resource/resource.h"
#include "ascii.h"

/* -------------------------------------------------------------------- */

static struct cimd_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

	char	*login,
		*password,
		*message_terminator;

	long 	login_delay;
	

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 0, NULL, 0,  "8N1",        0, 	&(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 0, NULL, 0,  "0708222901", 0, 	&(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 0, NULL, 0,  NULL,         9600, 	&(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,         45,   	&(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,         10,   	&(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,         10,   	&(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,         1,    	&(driver_env.def.max_deliver)  		},
		{ RESOURCE_STRING,  "SMS_login", 		0, 1, NULL, 0,  "EURO_CIMD",  0,   	&(driver_env.login)  			},
		{ RESOURCE_STRING,  "SMS_password", 		0, 1, NULL, 0,  "2062",       0,    	&(driver_env.password)  		},
		{ RESOURCE_STRING,  "SMS_message_terminator", 	0, 0, NULL, 0,  "\n",         0,    	&(driver_env.message_terminator)  	},
		{ RESOURCE_NUMERIC, "SMS_login_delay", 		0, 0, NULL, 0,  NULL,         2000000, 	&(driver_env.login_delay)  		},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,         0,    	NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define MSGTERM			(driver_env.message_terminator)
#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int CIMD_login(void);
static int CIMD_sendmessage(char *msisdn, char *message);
static int CIMD_send_disconnect(void);
static void CIMD_hangup(void);
static void calculate_checksum(char *checksum, char *ptr);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void calculate_checksum(char *checksum, char *ptr)
{
	unsigned int 	sum,
		high,
		low;
	

	sum = 0;
	while (ptr != checksum)
	{
	        sum += *ptr++;
	        sum &= 0x00FF;
	}

	high = (sum >> 4) & 0x000F;
	low  = (sum     ) & 0x000F;


	if (high <10)
	{	checksum[0] = '0' + high;
	}
	else
	{	checksum[0] = 'A' + (high -10);
	}

	if (low <10)
	{	checksum[1] = '0' + low;
	}
	else
	{	checksum[1] = 'A' + (low -10);
	}
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int CIMD_login(void)
{
	char 	buf[MAX_RESPONSE_BUFSIZE],
		message[512];



	if (expstr(FD, buf, "login: ", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received login request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No login request\n");
		
		CIMD_hangup();
		return -1;
	}


	lprintf(LOG_STANDARD, "Sending Service Login...\n");


	twrite(FD, "sc3cimd1\n", strlen("sc3cimd1\n"), WRITETIMEOUT);


	if (expstr(FD, buf, "CIMD", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
		{
			lprintf(LOG_STANDARD, "CIMD Service Detected\n");
		}
		else
		{	lprintf(LOG_STANDARD, "No CIMD Service Detected\n");
		
			CIMD_hangup();
			return -1;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "No CIMD Service Detected\n");
		
		CIMD_hangup();
		return -1;
	}

	libcommon_usleep(driver_env.login_delay);

	sprintf(message, "%c%s%c%s%c%s%c%s%c%s", S_STX,
						"01", 
						S_HT, 
						driver_env.login, 
						S_HT, 
						driver_env.password, 
						S_HT, 
						"XX",
						S_ETX,
						MSGTERM);

	calculate_checksum(&message[strlen(message) - (3 + strlen(MSGTERM))], message);


	twrite(FD, message, strlen(message), WRITETIMEOUT);


	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{

/*	should check for:
		<STX><ACK><HT>C:01<HT>chksum<ETX><LF>
*/

		if ((buf[0] == S_STX) &&
		    (buf[1] == S_ACK))
		{
			lprintf(LOG_STANDARD, "Completed CIMD Login\n");
		}
		else
		{	lprintf(LOG_STANDARD, "CIMD Login failed\n");
		
			CIMD_hangup();
			return -1;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "CIMD Login failed\n");
		
		CIMD_hangup();
		return -1;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int CIMD_sendmessage(char *msisdn, char *message)
{
	char 	buf[MAX_RESPONSE_BUFSIZE],
		nmessage[512];

	sprintf(nmessage, "%c%s%c%s%c%s%c%s%c%s", S_STX,
	                                          "03", 
	                                          S_HT, 
	                                          msisdn, 
	                                          S_HT, 
	                                          message,
	                                          S_HT,
	                                          "XX",
	                                          S_ETX,
	                                          MSGTERM);

	calculate_checksum(&nmessage[strlen(nmessage) - (3 + strlen(MSGTERM))], nmessage);

	twrite(FD, nmessage, strlen(nmessage), WRITETIMEOUT);


	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{

/*	should check for:
		<STX><ACK><TAB>C:04<TAB>status<TAB>err_desc<TAB>dest<TAB>scts<TAB>time<TAB>chksum<ETX><LF>
*/
		if ((buf[0] == S_STX) &&
		    (buf[1] == S_ACK))
		{	lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
		}
		else
		{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
			CIMD_hangup();
			return -1;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		CIMD_hangup();
		return -1;
	}



	lprintf(LOG_STANDARD, "Sending Acknowledgment...\n");

	sprintf(nmessage, "%c%c%c%s%c%s", S_STX,
	                                  S_ACK,
	                                  S_HT, 
	                                  "XX",
	                                  S_ETX,
	                                  MSGTERM);

	calculate_checksum(&nmessage[strlen(nmessage) - (3 + strlen(MSGTERM))], nmessage);

	twrite(FD, nmessage, strlen(nmessage), WRITETIMEOUT);


	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int CIMD_send_disconnect(void)
{
	char 	buf[MAX_RESPONSE_BUFSIZE],
		message[512];



	sprintf(message, "%c%s%c%s%c%s", 	S_STX,
					"02", 
					S_HT, 
					"XX",
					S_ETX,
					MSGTERM);

	calculate_checksum(&message[strlen(message) - (3 + strlen(MSGTERM))], message);

	twrite(FD, message, strlen(message), WRITETIMEOUT);


	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{

/*	should check for:
		<STX><ACK><TAB>C:02<TAB>time. Sess:time.Sub:n (M)<TAB>chksum<ETX><LF>
*/
		if ((buf[0] == S_STX) &&
		    (buf[1] == S_ACK))
		{	lprintf(LOG_STANDARD, "Received Disconnection Response\n");
		}
		else
		{	lprintf(LOG_STANDARD, "No Disconnection Response\n");
		
			CIMD_hangup();
			return -1;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "No Disconnection Response\n");
		
		CIMD_hangup();
		return -1;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void CIMD_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY cimd_device = {

	"CIMD",
	"1.1",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	CIMD_send_disconnect,
	default_multiple_counted_deliver,
	CIMD_sendmessage,
	CIMD_login
};





