#include <stdio.h>

int main(int argc, char **argv)
{
	int 	addr[4];
	char	ipaddress[512];
	

	strcpy(ipaddress, argv[1]);


        if ( sscanf(ipaddress, "%3d.%3d.%3d.%3d", &addr[0], &addr[1], &addr[2], &addr[3]) == 4 )
	{
		sprintf(ipaddress, "%3.3d%3.3d%3.3d%3.3d", addr[0], addr[1], addr[2], addr[3]);

		printf("%s\n", ipaddress);
	}
	else
	{	fprintf(stderr, "Error: bad format\n");
	}

	return 0;
}
