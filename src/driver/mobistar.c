/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* mobistar.c								*/
/*									*/
/*  Copyright (C) 1999,2000 yves.seynaeve@eyc.be 			*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  yves.seynaeve@eyc.be 	 					*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "\n";

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static struct proximus_env
{
	DRIVER_DEFAULT_ENV def;

} driver_env;


static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",  0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,   0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,   9600, 	 &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,   30, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,   10, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,   10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,   0, 	 &(driver_env.def.max_deliver) 	        },
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,   0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

#define STX "\02"          /* start character                                */
#define ETX "\03"          /* stop character                                 */
#define MAXPHONELEN 30     /* phone number maximum length                    */
#define MAXBUFLEN   300    /* maximum output buffer length                   */
#define MAXTEXTLEN  160     /* maximum SMS message length                     */
#define MODEM_NUMBER "026585382"
#define MAX_BUFSIZE 1024
#define EMOBISTAR_NORESPONSE -1

#define CMDFORMAT "00/00000/O/51/%s/%s/////////////////3//%s///0//////////"


/* -------------------------------------------------------------------- */

static char buf[MAX_BUFSIZE +1];

/* -------------------------------------------------------------------- */

static int MOBISTAR_login(void);
static int MOBISTAR_strtohex(char * , char *);
static int MOBISTAR_checksum(char *);
static void MOBISTAR_buildmessage(char *, char *, char *, char *);
static int  MOBISTAR_sendmessage(char *, char *);
static void MOBISTAR_hangup(void);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int MOBISTAR_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Mobistar Service Login\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Mobistar Service Login\n");
		
		MOBISTAR_hangup();
		return EMOBISTAR_NORESPONSE;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static void MOBISTAR_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* Convert a string to its  hexadecimal representation                  */
/* Returns the length of the string                                     */
/* -------------------------------------------------------------------- */

static int MOBISTAR_strtohex(char * strout , char * strin)
{ int i;

  for (i=0 ; i < strlen(strin); i++ )
  { sms_snprintf(&strout[2*i],2,"%2X",strin[i]);
  }
  return strlen(strout);
}

/* -------------------------------------------------------------------- */
/*   Compute checksum                                                   */
/* -------------------------------------------------------------------- */

static int MOBISTAR_checksum(char * msg)
{ int cpt;
  int i;

  cpt=0;
  for (i=0;i<strlen(msg);i++)
  {  cpt=cpt+msg[i];
  }

  return cpt%256;
}


/* -------------------------------------------------------------------- */
/* Build message with stx and etx                                       */
/* -------------------------------------------------------------------- */

static void  MOBISTAR_buildmessage(char * outmsg , char * msgtext , char * phcaller , char * phdest)
{ char  hexmsg[2*MAXTEXTLEN+1];     /* Message in hex representation */
  int   hexmsglen;                  /* Message in hex representation length */
  int   outmsglen;                  /* Length of the message to be sent     */
  int   chksum;                     /* Checksum of the message              */
  char  coutmsglen[6];              /* Output buffer length with checksum   */
  char  outmsgtmp[MAXBUFLEN+1];

  hexmsglen=MOBISTAR_strtohex(hexmsg,msgtext);

  outmsglen=sms_snprintf(outmsgtmp,MAXBUFLEN,CMDFORMAT,phdest,phcaller,hexmsg);

  sms_snprintf(coutmsglen,5,"%05d",outmsglen+2); /* +2 chksum character */
  memcpy(outmsgtmp+3,coutmsglen,5);

  chksum=MOBISTAR_checksum(outmsgtmp);
  sms_snprintf(outmsg,MAXBUFLEN,"%s%s%2X%s",STX,outmsgtmp,chksum,ETX);
}



/* -------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
static int MOBISTAR_sendmessage(char *msisdn, char *message)
{
	char 	mobistar_message[MAXBUFLEN+1];


	MOBISTAR_buildmessage(mobistar_message, message,MODEM_NUMBER,msisdn);
	
	twrite(FD, mobistar_message, strlen(mobistar_message), WRITETIMEOUT);

	if (expstr(FD, buf, ETX, MAX_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Response\n"
		                      "SMSC Respsonse: %s\n", buf);
	}
	else
	{	lprintf(LOG_STANDARD, "No Message or Wrong Response\n");

		MOBISTAR_hangup();
		return EMOBISTAR_NORESPONSE;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY mobistar_device = {

	"MOBISTAR",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	default_send_disconnect,
	default_single_deliver,
	MOBISTAR_sendmessage,
	MOBISTAR_login
};

