/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* snpp.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static struct snpp_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 0, NULL, 0,  NULL,        0,  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 0, NULL, 0,  NULL,        0,  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 0, NULL, 0,  NULL,        0,  &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,        30, &(driver_env.def.deliver_timeout)  },
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,        10, &(driver_env.def.timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,        10, &(driver_env.def.write_timeout)  	},

		{ RESOURCE_STRING,  "SMS_server_name", 		0, 1, NULL, 0,  "localhost", 0,  &(driver_env.def.server_name)		},
		{ RESOURCE_NUMERIC, "SMS_server_port", 		0, 1, NULL, 0,  NULL, 	     444, &(driver_env.def.server_port)		},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,        0,  NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int SNPP_login(void);
static int SNPP_sendmessage(char *msisdn, char *message);
static int SNPP_send_disconnect(void);
static void SNPP_hangup(void);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int SNPP_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		if (strncmp(buf, "220", 3) == 0)
		{
			lprintf(LOG_STANDARD, "SNPP Service Login\n");
			lprintf(LOG_VERBOSE, "SNPP Response: %s", buf);		
		}
		else
		{	lprintf(LOG_STANDARD, "SNPP Service Login Failed - Bad response\n");

			SNPP_hangup();
			return ESNPP_NORESPONSE;
		}
	}
	else
	{
		lprintf(LOG_STANDARD, "SNPP Service Login Failed\n");
		
		SNPP_hangup();
		return ESNPP_NORESPONSE;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int SNPP_sendmessage(char *msisdn, char *message)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	twrite(FD, "PAGE ", strlen("PAGE "), WRITETIMEOUT);
	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\n", strlen("\n"), WRITETIMEOUT);

	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		if (strncmp(buf, "250", 3) == 0)
		{
			lprintf(LOG_STANDARD, "Pager ID Accepted\n");
			lprintf(LOG_VERBOSE, "SNPP Response: %s", buf);		
		}
		else
		{	lprintf(LOG_STANDARD, "Pager ID NOT Accepted\n");

			SNPP_hangup();
			return ESNPP_NONUMBER;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "Pager ID NOT Accepted\n");

		SNPP_hangup();
		return ESNPP_NONUMBER;
	}


	twrite(FD, "MESS ", strlen("MESS "), WRITETIMEOUT);
	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\n", strlen("\n"), WRITETIMEOUT);


	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		if (strncmp(buf, "250", 3) == 0)
		{
			lprintf(LOG_STANDARD, "Message Accepted\n");
			lprintf(LOG_VERBOSE, "SNPP Response: %s", buf);		
		}
		else
		{	lprintf(LOG_STANDARD, "Message NOT Accepted\n");

			SNPP_hangup();
			return ESNPP_NOMESSAGE;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "Message NOT Accepted\n");

		SNPP_hangup();
		return ESNPP_NOMESSAGE;
	}



	twrite(FD, "SEND\n", strlen("SEND\n"), WRITETIMEOUT);

	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		if (strncmp(buf, "250", 3) == 0)
		{
			lprintf(LOG_STANDARD, "Message Sent\n");
			lprintf(LOG_VERBOSE, "SNPP Response: %s", buf);		
		}
		else
		{	lprintf(LOG_STANDARD, "Message NOT Sent\n");

			SNPP_hangup();
			return ESNPP_NODELIVERY;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "Message NOT Sent\n");

		SNPP_hangup();
		return ESNPP_NODELIVERY;
	}


	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int SNPP_send_disconnect(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	twrite(FD, "QUIT\n", strlen("QUIT\n"), WRITETIMEOUT);

	if (expstr(FD, buf, "\n", MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		if (strncmp(buf, "221", 3) == 0)
		{
			lprintf(LOG_STANDARD, "Sent Diconnection\n");
			lprintf(LOG_VERBOSE, "SNPP Response: %s", buf);		
		}
		else
		{	lprintf(LOG_STANDARD, "Failed to send Disconnection\n");

			SNPP_hangup();
			return ESNPP_NODISCONNECT;
		}
	}
	else
	{	lprintf(LOG_STANDARD, "Failed to send Disconnection\n");

		SNPP_hangup();
		return ESNPP_NODISCONNECT;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void SNPP_hangup(void)
{	TCPIP_disconnect(FD);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY snpp_device = {

	"SNPP",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_always_true,
	default_tcpip_connect,
	default_tcpip_disconnect,
	SNPP_send_disconnect,
	default_single_deliver,
	SNPP_sendmessage,
	SNPP_login
};




