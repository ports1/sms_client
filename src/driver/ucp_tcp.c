/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* ucp_tcp.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "ascii.h"
#include "ia5table.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static struct ucp_tcp_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

	char 	*ipaddress,
		*password;

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 0, NULL, 0,  NULL,        0,  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 0, NULL, 0,  NULL,        0,  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 0, NULL, 0,  NULL,        0,  &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,        30, &(driver_env.def.deliver_timeout)  },
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,        10, &(driver_env.def.timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,        10, &(driver_env.def.write_timeout)  	},

		{ RESOURCE_STRING,  "SMS_ipaddress", 		0, 1, NULL, 0,  "", 	    0,    &(driver_env.ipaddress)		},
		{ RESOURCE_STRING,  "SMS_password", 		0, 1, NULL, 0,  "", 	    0,    &(driver_env.password)		},

		{ RESOURCE_STRING,  "SMS_server_name", 		0, 1, NULL, 0,  "localhost", 0,  &(driver_env.def.server_name)		},
		{ RESOURCE_NUMERIC, "SMS_server_port", 		0, 1, NULL, 0,  NULL, 	     500, &(driver_env.def.server_port)		},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,        0,  NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */




#include "libucp.c"




/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static int UCP_TCP_init(char *mservice, DEVICE_ENTRY *device);
static int UCP_TCP_login(void);
static int UCP_TCP_sendmessage(char *msisdn, char *message);
static void UCP_TCP_hangup(void);


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int UCP_TCP_init(char *mservice, DEVICE_ENTRY *device)
{
	char	*ptr;
	int 	addr[4];


	if ( default_init(mservice, device) == -1 )
	{	return -1;
	}


        if ( sscanf(driver_env.ipaddress, "%3d.%3d.%3d.%3d", &addr[0], &addr[1], &addr[2], &addr[3]) == 4 )
	{
		sprintf(driver_env.ipaddress, "%3.3d%3.3d%3.3d%3.3d", addr[0], addr[1], addr[2], addr[3]);
	}
	else
	{	lprintf(LOG_ERROR, "IP Address in incorrect format\n");
		return -1;
	}

    /* ------------------------------------ */
    /* Convert password to ia5 format       */
    /* ------------------------------------ */

    ptr = driver_env.password;
    while (*ptr != '\0')
    {
        *ptr = toia5(*ptr);
        ptr++;
    }

	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int UCP_TCP_login(void)
{
    char   buf[MAX_RESPONSE_BUFSIZE],
           *ucpMessage,
           *ptr;

    int    result;


    /* ------------------------------------ */
    /* Convert password to ia5 format       */
    /* ------------------------------------ */

    ptr = driver_env.password;
    while (*ptr != '\0')
    {
        *ptr = toia5(*ptr);
        ptr++;
    }

    /* ------------------------------------ */


    ucpMessage = UCP_CallInputOp60( driver_env.ipaddress, driver_env.password );

    twrite(FD, ucpMessage, strlen(ucpMessage), WRITETIMEOUT);

    if( expstr(FD, buf, "\03", MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
    {
        lprintf(LOG_STANDARD, "SMSC Login Respsonse: %s\n", buf);

        result = UCP_parse_response(buf);
        if     (result == 0)
        {
            lprintf(LOG_STANDARD, "Login accepted\n");
        }
        else if(result == 1)
        {
            lprintf(LOG_ERROR, "Login rejected\n");
            UCP_TCP_hangup();
            return EUCP_ACKFAILED;
        }
        else
        {
            lprintf(LOG_ERROR, "Bad Login acknowledgement\n");
            UCP_TCP_hangup();
            return EUCP_BADACK;
        }
     }
     else
     {
        lprintf(LOG_ERROR, "No Login Response\n");
        UCP_TCP_hangup();
        return EUCP_NORESPONSE;
     }

     return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int UCP_TCP_sendmessage(char *msisdn, char *message)
{
    char   buf[MAX_RESPONSE_BUFSIZE],
           *ucpMessage,
           *msg;


    int    result;

    /* ------------------------------------ */
    /* Convert message to ia5 format        */
    /* This should be done prior to         */
    /* entering this function.              */
    /* ------------------------------------ */

    msg = message;
    while (*msg != '\0')
    {
        *msg = toia5(*msg);
        msg++;
    }

    /* ------------------------------------ */

#if 0
    ucpMessage = UCP_CallInputOp01(msisdn, message);
#else
    ucpMessage = UCP_CallInputOp51(msisdn, message, driver_env.ipaddress);
#endif

    twrite(FD, ucpMessage, strlen(ucpMessage), WRITETIMEOUT);

    if( expstr(FD, buf, "\03", MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
    {
        lprintf(LOG_STANDARD, "SMSC Respsonse: %s\n", buf);

        result = UCP_parse_response(buf);
        if     (result == 0)
        {
            lprintf(LOG_STANDARD, "Message accepted\n");
        }
        else if(result == 1)
        {
            lprintf(LOG_ERROR, "Message rejected\n");
            UCP_TCP_hangup();
            return EUCP_ACKFAILED;
        }
        else
        {
            lprintf(LOG_ERROR, "Bad message acknowledgement\n");
            UCP_TCP_hangup();
            return EUCP_BADACK;
        }
     }
     else
     {
        lprintf(LOG_ERROR, "No Message Response\n");
        UCP_TCP_hangup();
        return EUCP_NORESPONSE;
     }

     return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void UCP_TCP_hangup(void)
{	TCPIP_disconnect(FD);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY ucp_tcp_device = {

	"UCP_TCP",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	UCP_TCP_init,
	default_main,
	default_validate_always_true,
	default_tcpip_connect,
	default_tcpip_disconnect,
        default_send_disconnect,
	default_single_deliver,
	UCP_TCP_sendmessage,
	UCP_TCP_login
};




