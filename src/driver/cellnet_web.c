/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* cellnet_web.c							*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "comms/http.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static struct cellnet_web_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

	char 	*url,
		*username,
		*password;

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",      0, 	  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       9600, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       3,    &(driver_env.def.max_deliver)  	},

		{ RESOURCE_STRING,  "SMS_url", 			0, 1, NULL, 0,  "http://www.genie.co.uk/gmail_mdl/externalsend.hnspl", 	    0,    &(driver_env.url)			},
		{ RESOURCE_STRING,  "SMS_username", 		0, 1, NULL, 0,  "", 	    0,    &(driver_env.username)			},
		{ RESOURCE_STRING,  "SMS_password", 		0, 1, NULL, 0,  "", 	    0,    &(driver_env.password)			},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0,    NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int CELLNET_WEB_dial(DRIVER_DEFAULT_ENV *env);
static int CELLNET_WEB_sendmessage(char *msisdn, char *message);
static void CELLNET_WEB_hangup(DRIVER_DEFAULT_ENV *env);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int CELLNET_WEB_dial(DRIVER_DEFAULT_ENV *env)
{
	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int CELLNET_WEB_sendmessage(char *msisdn, char *message)
{
	char 	*escaped_message,
		data[1024],
		*document;

	int	document_len;


	escaped_message = escape_input(message);
	if (escaped_message == NULL)
	{
		exit(-1);
	}

	sms_snprintf(data, 1024, "ac=guest&username=%s&password=%s&TO=%s"
	              "&MSG_MESSAGE=%s&submit=Send&count=1",
	              driver_env.username, 
	              driver_env.password,
	              msisdn, 
	              escaped_message);

	free(escaped_message);



	document = get_document(driver_env.url, &document_len, "POST", data, NULL, NULL);
	if (document != NULL)
	{
		lprintf(LOG_VERYVERBOSE, "%s\n", document);

		if (strstr(document, "has been queued") != NULL)
		{
			lprintf(LOG_STANDARD, "Your message has been successfully queued for transmission\n");
		}
		else
		if (strstr(document, "couldn't send your message") != NULL)
		{
			lprintf(LOG_ERROR, "Trying to Deliver message - Genie didn't like your submission\n");

			free(document);
			return -1;
		}
		else
		{	lprintf(LOG_ERROR, "Trying to Deliver message - Unexpected response\n");
	
			free(document);
			return -1;
		}

		free(document);
	}
	else
	{	lprintf(LOG_ERROR, "Trying to Deliver message - Failed to connect\n");
		return -1;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void CELLNET_WEB_hangup(DRIVER_DEFAULT_ENV *env)
{	
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY cellnet_web_device = {

	"CELLNET_WEB",
	"1.2",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	CELLNET_WEB_dial,
	CELLNET_WEB_hangup,
	default_send_disconnect,
	default_single_deliver,
	CELLNET_WEB_sendmessage,
	default_login
};



