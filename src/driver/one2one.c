/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* one2one.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id: one2one.c,v 5.1 1998/02/01 07:10:39 root Exp root $
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "ID=";
static char ACK2[] = "Welcome to the One 2 One Short Message Service\r\n";
static char ACK3[] = "Password: ";

static char ACK4[] = "Please enter below the message you wish to send\r\n";
static char ACK5[] = "Enter the One 2 One phone number to receive your message: ";
static char ACK6[] = "Goodbye (you should hangup now)";

/* -------------------------------------------------------------------- */

static struct one2one_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "7E1",     0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,      0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,      1200, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,      30, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,      10, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,      10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,      0,    &(driver_env.def.max_deliver)  	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,      0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static void ONE2ONE_hangup(void);
static int ONE2ONE_login(void);
static int ONE2ONE_sendmessage(char *msisdn, char *message);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int ONE2ONE_login(void)
{
	char 	buf[MAX_RESPONSE_BUFSIZE];

	twrite(FD, "\r", strlen("\r"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received one2one ID Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No one2one ID Request\n");
		
		ONE2ONE_hangup();
		return EONE2ONE_NORESPONSE;
	}

	twrite(FD, "A\r", strlen("A\r"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received one2one Welcome\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No one2one Welcome\n");
		
		ONE2ONE_hangup();
		return EONE2ONE_NORESPONSE;
	}

	if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received one2one Password Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No one2one Password Request\n");
		
		ONE2ONE_hangup();
		return EONE2ONE_NORESPONSE;
	}

	twrite(FD, "\r", strlen("\r"), WRITETIMEOUT);

	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int ONE2ONE_sendmessage(char *msisdn, char *message)
{
	char 	buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received one2one Message Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No one2one Message Request\n");
		
		ONE2ONE_hangup();
		return EONE2ONE_NONUMBER;
	}


	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r", strlen("\r"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK5, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Request\n");
		
		ONE2ONE_hangup();
		return EONE2ONE_NOMESSAGE;
	}

	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r", strlen("\r"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK6, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		ONE2ONE_hangup();
		return EONE2ONE_NODELIVERY;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void ONE2ONE_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY one2one_device = {

	"ONE2ONE",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	default_send_disconnect,
	default_multiple_counted_deliver,
	ONE2ONE_sendmessage,
	ONE2ONE_login
};


