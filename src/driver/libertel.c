/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* libertel.c								*/
/*									*/
/*  Copyright (C) 1998,1999 Aart Koelewijn				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  jhwevers@telekabel.nl						*/
/*  aart@mtack.xs4all.nl						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id: libertel.c,v 0.1 1998/04/02 22:49:15 aart
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "Welkom bij LIBERTEL Text-PC\r\n"
                     "Als u vragen heeft over deze dienst kunt u bellen naar 0800-0560\r\n";
static char ACK2[] = "Typ het mobiele telefoonnummer in volgens het internationale formaat, \r\n"
                     "bijvoorbeeld 31654XXXXXX, gevolgd door enter (of druk op enter om te stoppen)\r\n"
                     ">";
                     
static char ACK3[] = "Typ uw bericht in, (maximaal 160 tekens), gevolgd door enter\r\n"
                     ">";

static char ACK4[] = "Bericht geaccepteerd\r\n";

/* -------------------------------------------------------------------- */

static struct libertel_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",      0, 	  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       1200, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       0,    &(driver_env.def.max_deliver)  	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0, 	  NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static void LIBERTEL_hangup(void);
static int LIBERTEL_sendmessage(char *msisdn, char *message);
static int LIBERTEL_login(void);
static int LIBERTEL_send_disconnect(void);
	
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void LIBERTEL_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int LIBERTEL_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];

	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Libertel Service Login\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Libertel Service Response\n");
		
		LIBERTEL_hangup();
		return ELIBERTEL_NORESPONSE;
	}

	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int LIBERTEL_send_disconnect(void)
{
	twrite(FD, "\r\n", strlen("\r\n"), TIMEOUT);
	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int LIBERTEL_sendmessage(char *msisdn, char *message)
{
	char buf[MAX_RESPONSE_BUFSIZE];

	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Request\n");
		
		LIBERTEL_hangup();
		return ELIBERTEL_NONUMBER;
	}


	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Request\n");
		
		LIBERTEL_hangup();
		return ELIBERTEL_NOMESSAGE;
	}


	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	
	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		LIBERTEL_hangup();
		return ELIBERTEL_NODELIVERY;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY libertel_device = {

	"LIBERTEL",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	LIBERTEL_send_disconnect,
	default_multiple_counted_deliver,
	LIBERTEL_sendmessage,
	LIBERTEL_login
};

