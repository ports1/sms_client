/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* driver.h								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */


#if !defined(_DRIVER_H_)
#define _DRIVER_H_

#include "resource/resource.h"

/* -------------------------------------------------------------------- */

typedef struct device_entry_struct DEVICE_ENTRY;
typedef struct driver_default_env_struct DRIVER_DEFAULT_ENV;

/* -------------------------------------------------------------------- */

struct driver_default_env_struct {


	char 	*comms_params,
		*flow_control,
		*centre_number,
		*server_name;


	int	fd,
		connection_status;

	DEVICE_ENTRY *device;

	long	baud, 
		deliver_timeout,
		timeout,
		write_timeout,
		num_sent,
		max_deliver,
		server_port;
};


/* -------------------------------------------------------------------- */

struct device_entry_struct {

	char 	*name,
	 	*version_string;

	RESOURCE 
		*resource_list;

	DRIVER_DEFAULT_ENV 
		*env;

	int 	(* init)(char *service, DEVICE_ENTRY *device);

	void 	(* main)(void *list, 
                         void *(* get_first)(void *list), 
                         void *(* get_next)(void *list), 
                         char *(* get_number)(void *node), 
                         char *(* get_message)(void *node),
                         void (* set_delivery)(void *node, int result),
                         DRIVER_DEFAULT_ENV *env);

	int 	(* validate_id)(char *id);
	int 	(* dial)(DRIVER_DEFAULT_ENV *env);
	void 	(* hangup)(DRIVER_DEFAULT_ENV *env);
	int 	(* disconnect)(void);
	int 	(* deliver)(char *msisdn, char *message, DRIVER_DEFAULT_ENV *env);
	int 	(* sendmessage)(char *msisdn, char *message);
	int 	(* login)(void);
};

/* -------------------------------------------------------------------- */

extern DEVICE_ENTRY tap_device;
extern DEVICE_ENTRY vodafone_device;
extern DEVICE_ENTRY orange_device;
extern DEVICE_ENTRY pageone_device;
extern DEVICE_ENTRY vodacom_device;
extern DEVICE_ENTRY mtn_device;
extern DEVICE_ENTRY one2one_device;
extern DEVICE_ENTRY libertel_device;
extern DEVICE_ENTRY tim_device;
extern DEVICE_ENTRY snpp_device;
extern DEVICE_ENTRY cimd_device;
extern DEVICE_ENTRY proximus_device;
extern DEVICE_ENTRY vodapage_block_device;
extern DEVICE_ENTRY kpn_device;
extern DEVICE_ENTRY answer_device;
extern DEVICE_ENTRY generic_device;
extern DEVICE_ENTRY ucp_device;
extern DEVICE_ENTRY ucp_tcp_device;
extern DEVICE_ENTRY www_device;
extern DEVICE_ENTRY orange_web_device;
extern DEVICE_ENTRY lycos_web_device;
extern DEVICE_ENTRY cellnet_web_device;
extern DEVICE_ENTRY proximus_web_device;
extern DEVICE_ENTRY att_web_device;
extern DEVICE_ENTRY nextel_web_device;
extern DEVICE_ENTRY pagenet_web_device;
extern DEVICE_ENTRY mobistar_device;

/* -------------------------------------------------------------------- */

extern DEVICE_ENTRY *device_list[];

/* -------------------------------------------------------------------- */

#define SERVICE_NOT_CONNECTED 	0
#define SERVICE_CONNECTED 	1

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

#define MAX_RESPONSE_BUFSIZE	8096

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

DEVICE_ENTRY *get_device(char *name);
void display_drivers(void);

void default_main(void *list, 
                  void *(* get_first)(void *list), 
                  void *(* get_next)(void *list), 
                  char *(* get_number)(void *node), 
                  char *(* get_message)(void *node),
                  void (* set_delivery)(void *node, int result),
                  DRIVER_DEFAULT_ENV *env);

int default_init(char *mservice, DEVICE_ENTRY *device);
int default_dial(DRIVER_DEFAULT_ENV *env);
int default_tcpip_connect(DRIVER_DEFAULT_ENV *env);
int default_multiple_deliver(char *msisdn, char *message, DRIVER_DEFAULT_ENV *env);
int default_single_deliver(char *msisdn, char *message, DRIVER_DEFAULT_ENV *env);
int default_multiple_counted_deliver(char *msisdn, char *message, DRIVER_DEFAULT_ENV *env);
int default_send_disconnect(void);
int default_validate_numeric_id(char *ptr);
int default_validate_always_true(char *id);
int default_login(void);
int default_sendmessage(char *msisdn, char *message);

void default_hangup(DRIVER_DEFAULT_ENV *env);
void default_tcpip_disconnect(DRIVER_DEFAULT_ENV *env);

char *get_protocol(char *service);

/* -------------------------------------------------------------------- */
#endif
