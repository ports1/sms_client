/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* vodafone.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999,2000 Angelo Masci			*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/*  Suggested patch for Australian Telenotes support by 		*/
/*     Jeremy Laidman	<JLaidman@AUUG.org.au> 13-Jan-1999		*/
/*  									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

#if 0
static char ACK1[] = "WELCOME TO THE VODAFONE PC TEXT MESSAGING SERVICE.\r\n";
#else
static char ACK1[] = "WELCOME TO THE VODAFONE";
#endif

static char ACK2[] = "FOLLOWED BY RETURN, (OR RETURN TO QUIT).\r\n"
	             "\r\n"
	             ">";
	             
static char ACK3[] = "FOLLOWED BY RETURN.\r\n"
		     "\r\n"
		     ">";

static char ACK4[] = "MESSAGE ACCEPTED.\r\n";
		     
/* -------------------------------------------------------------------- */

static struct vodafone_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{ 
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "7E1",      0, 	  &(driver_env.def.comms_params)  	 },
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	 },
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       1200, &(driver_env.def.baud)  		 },
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	 },
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		 },
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	 },
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       1,    &(driver_env.def.max_deliver)  	 },
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0, 	  NULL  				 }
	};


/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int VODAFONE_login(void);
static int VODAFONE_sendmessage(char *msisdn, char *message);
static void VODAFONE_hangup(void);
static int VODAFONE_send_disconnect(void);
	
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int VODAFONE_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];

	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Vodafone Service Login\n");
	}
	else
	{
		lprintf(LOG_STANDARD, "No Vodafone Service Response\n");
		
		VODAFONE_hangup();
		return EVODAFONE_NORESPONSE;
	}

	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int VODAFONE_sendmessage(char *msisdn, char *message)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Request\n");
	}
	else
	{
		lprintf(LOG_STANDARD, "No Number Request\n");
		
		VODAFONE_hangup();
		return EVODAFONE_NONUMBER;
	}


	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);


	if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Request\n");
	}
	else
	{
		lprintf(LOG_STANDARD, "No Message Request\n");
		
		VODAFONE_hangup();
		return EVODAFONE_NOMESSAGE;
	}

	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	
	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{
		lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		VODAFONE_hangup();
		return EVODAFONE_NODELIVERY;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int VODAFONE_send_disconnect(void)
{
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void VODAFONE_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY vodafone_device = {

	"VODAFONE",
	"1.1",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	VODAFONE_send_disconnect,
	default_single_deliver,
	VODAFONE_sendmessage,
	VODAFONE_login
};


