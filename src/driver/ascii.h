/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* ascii.h								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#define S_NUL	0x00
#define S_SOH	0x01
#define S_STX	0x02
#define S_ETX	0x03
#define S_EOT	0x04
#define S_ENQ	0x05
#define S_ACK 	0x06
#define S_BEL 	0x07
#define S_BS 	0x08
#define S_HT 	0x09
#define S_LF	0x0A
#define S_VT	0x0B
#define S_NP	0x0C
#define S_CR	0x0D
#define S_SO	0x0E
#define S_SI	0x0F
#define S_DLE	0x10
#define S_DC1	0x11
#define S_DC2	0x12
#define S_DC3	0x13
#define S_DC4	0x14
#define S_NAK	0x15
#define S_SYN	0x16
#define S_ETB	0x17
#define S_CAN	0x18
#define S_EM	0x19
#define S_SUB	0x1A
#define S_ESC	0x1B
#define S_FS	0x1C
#define S_GS	0x1D
#define S_RS	0x1E
#define S_US	0x1F

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

