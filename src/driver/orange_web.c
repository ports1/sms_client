/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* orange_web.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "comms/http.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static struct orange_web_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

	char 	*url,
		*username,
		*password;

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",      0, 	  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       9600, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       3,    &(driver_env.def.max_deliver)  	},

		{ RESOURCE_STRING,  "SMS_url", 			0, 1, NULL, 0,  "http://www.uk.orange.net/cgi-bin/register/sms.pl", 	    0,    &(driver_env.url)			},
		{ RESOURCE_STRING,  "SMS_username", 		0, 1, NULL, 0,  "", 	    0,    &(driver_env.username)		},
		{ RESOURCE_STRING,  "SMS_password", 		0, 1, NULL, 0,  "", 	    0,    &(driver_env.password)		},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0,    NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int ORANGE_WEB_dial(DRIVER_DEFAULT_ENV *env);
static int ORANGE_WEB_sendmessage(char *msisdn, char *message);
static void ORANGE_WEB_hangup(DRIVER_DEFAULT_ENV *env);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int ORANGE_WEB_dial(DRIVER_DEFAULT_ENV *env)
{
	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static char *messagetype[] = { 

	"0973", 
	"0976", 
	"0966", 
	"07967", 
	"07970",
	"07971",
	"07974",
	"07977",
	"0941",
	NULL
};



static int ORANGE_WEB_sendmessage(char *msisdn, char *message)
{
	int	document_len,
		i;

	char	*src, 
		*dst, 
		data[1024],
		*document,
		*escaped_message,
		*short_number,
		*auth,
		*number_prefix;


	/* ------------------------------------------------------------ */

	auth = (char *)malloc(sizeof(char) * (strlen(driver_env.username) + 1 + strlen(driver_env.password) +1));
	if (auth == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	sms_snprintf(auth, (strlen(driver_env.username) + 1 + strlen(driver_env.password) +1), "%s:%s", driver_env.username, driver_env.password);

	/* ------------------------------------------------------------ */

	short_number = strdup(msisdn);
	if (short_number == NULL)
	{
		exit(-1);
	}


	for (i=0; messagetype[i] != NULL; i++)
	{
		if (strncmp(messagetype[i], short_number, strlen(messagetype[i])) == 0)
		{
			break;
		} 
	}

	if (messagetype[i] == NULL)
	{
		number_prefix = "Local";
	}
	else
	{	number_prefix = messagetype[i];

		dst = short_number;
		src = short_number + strlen(messagetype[i]);

		while ((*dst = *src))
		{
			dst++;
			src++;
		}
	}

	escaped_message = escape_input(message);
	if (escaped_message == NULL)
	{
		exit(-1);
	}

	/* ------------------------------------------------------------ */


	sms_snprintf(data, 1024, "FRAMES=++&messagetype=%s&number=%s&message=%s",
	        number_prefix,
	        short_number,
	        escaped_message);

	free(short_number);
	free(escaped_message);


	/* ------------------------------------------------------------ */


	document = get_document(driver_env.url, &document_len, "POST", data, auth, NULL);
	if (document != NULL)
	{
		lprintf(LOG_VERYVERBOSE, "%s\n", document);

		if (strstr(document, "has been sent successfully") != NULL)
		{
			lprintf(LOG_STANDARD, "Your message has been successfully queued for transmission\n");
		}
		else
		if (strstr(document, "your message could not be sent") != NULL)
		{
			lprintf(LOG_ERROR, "Trying to Deliver message - Orange didn't like your submission\n");

			free(document);
			return -1;
		}
		else
		{	lprintf(LOG_ERROR, "Trying to Deliver message - Unexpected response\n");

			free(document);
			return -1;
		}

		free(document);
	}
	else
	{
		lprintf(LOG_ERROR, "Trying to Deliver message - Failed to connect\n");
		return -1;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void ORANGE_WEB_hangup(DRIVER_DEFAULT_ENV *env)
{	
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY orange_web_device = {

	"ORANGE_WEB",
	"1.1",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	ORANGE_WEB_dial,
	ORANGE_WEB_hangup,
	default_send_disconnect,
	default_single_deliver,
	ORANGE_WEB_sendmessage,
	default_login
};



