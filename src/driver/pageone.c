/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* pageone.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id: pageone.c,v 5.1 1998/02/01 07:10:39 root Exp root $
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "ID=\r\n";
static char ACK2[] = "ENTER SUBSCRIBER NUMBER\n\r";
static char ACK4[] = "ENTER MESSAGE: ";
static char ACK5[] = "PAGE ACCEPTED\n\r";

/* -------------------------------------------------------------------- */

static struct pageone_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "7E1",     0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,      0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,      9600, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,      30, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,      10, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,      10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,      0,    &(driver_env.def.max_deliver)  	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,      0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

static int PAGEONE_login(void);
static int PAGEONE_sendmessage(char *msisdn, char *message);
static void PAGEONE_hangup(void);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int PAGEONE_login(void)
{
	char 	buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Pageone ID Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Pageone ID Request\n");
		
		PAGEONE_hangup();
		return EPAGEONE_NORESPONSE;
	}

#if 0
	twrite(FD, "PG1\r", strlen("PG1\r"), WRITETIMEOUT);
#else
	twrite(FD, "M\r", strlen("M\r"), WRITETIMEOUT);
#endif

	return 0;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int PAGEONE_sendmessage(char *msisdn, char *message)
{
	char 	buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Pageone Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Pageone Number Request\n");
		
		PAGEONE_hangup();
		return EPAGEONE_NONUMBER;
	}

	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r", strlen("\r"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Request\n");
		
		PAGEONE_hangup();
		return EPAGEONE_NOMESSAGE;
	}

	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r", strlen("\r"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK5, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		PAGEONE_hangup();
		return EPAGEONE_NODELIVERY;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void PAGEONE_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY pageone_device = {

	"PAGEONE",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_always_true,
	default_dial,
	default_hangup,
	default_send_disconnect,
	default_multiple_counted_deliver,
	PAGEONE_sendmessage,
	PAGEONE_login
};


