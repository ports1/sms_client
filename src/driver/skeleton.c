/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* skeleton.c								*/
/*									*/
/*  Copyright (C) 1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "Welecome to Skeleton Server\r\n";
static char ACK2[] = "Enter you number\r\n";
static char ACK3[] = "Enter you message\r\n";
static char ACK4[] = "Thank You.\r\n";
		     
/* -------------------------------------------------------------------- */
/* The 'env' structure contains Service level data to be used for	*/
/* sending a message.							*/
/* -------------------------------------------------------------------- */

static struct skeleton_env
{
	DRIVER_DEFAULT_ENV def;

	/* Place any extended driver	*/ 
	/* variables here 		*/

} driver_env;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",      0, 	  &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,       0, 	  &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,       1200, &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,       30,   &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,       10,   &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,       0,    &(driver_env.def.max_deliver)  	 },
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0, 	  NULL  				}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */
/* The following defines are used to signify errors			*/
/* occuring during the dialogue between the driver and the		*/
/* service centre. This number will be returned as the delivery		*/
/* value for the message that was aborted				*/
/*									*/
/* The defines MUST be placed in 'error.h'				*/

#define ESKELETON_NONUMBER 	250
#define ESKELETON_NOMESSAGE	251
#define ESKELETON_NODELIVERY	252

/* -------------------------------------------------------------------- */

static void SKELETON_hangup(void);
static int SKELETON_sendmessage(char *msisdn, char *message);
	
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void SKELETON_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int SKELETON_sendmessage(char *msisdn, char *message)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK2, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Number Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Number Request\n");
		
		SKELETON_hangup();
		return ESKELETON_NONUMBER;
	}


	twrite(FD, msisdn, strlen(msisdn), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);

	if (expstr(FD, buf, ACK3, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Request\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Request\n");
		
		SKELETON_hangup();
		return ESKELETON_NOMESSAGE;
	}


	twrite(FD, message, strlen(message), WRITETIMEOUT);
	twrite(FD, "\r\n", strlen("\r\n"), WRITETIMEOUT);
	
	if (expstr(FD, buf, ACK4, MAX_RESPONSE_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Delivery Response\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Delivery Response\n");
		
		SKELETON_hangup();
		return ESKELETON_NODELIVERY;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* The following structure is used by core driver code. 		*/
/* The diagram below shows the call sequence of the functions.		*/
/* -------------------------------------------------------------------- */
DEVICE_ENTRY skeleton_device = {

	"SKELETON",
	"1.0",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,				/* Init			*/ 
	default_main,				/* Main			*/
	default_validate_numeric_id,		/* Validation		*/
	default_dial,				/* Dial			*/
	default_hangup,				/* Hangup		*/
	default_send_disconnect,		/* Disconnect		*/
	default_single_deliver,			/* Deliver 		*/
	SKELETON_sendmessage,			/* Send			*/
	default_login				/* Login		*/
};

/* -------------------------------------------------------------------- */
/*                                                                      */
/*       ----------                                                     */
/*       |  Main  |                                                     */
/*       ----+-----                                                     */
/*           |                                                          */
/*           |                                                          */
/*           | For Each Addressed Message                               */
/*      -----+-----                                                     */
/*      | Deliver |                                                     */
/*      -----+-----                                                     */
/*           |                                                          */
/*      +----+------+----------+-------------+--------------+           */
/*      |           |          |             |              |           */
/*   ---+----   ----+----   ---+----   ------+-------   ----+-----      */
/*   | Dial |   | Login |   | Send |   | Disconnect |   | Hangup |      */
/*   --------   ---------   --------   --------------   ----------      */
/*                                                                      */
/*                                                                      */
/* Note. Your driver can supply functions to be used in place of        */
/*       all the default_driver functions.                              */
/*                                                                      */
/* -------------------------------------------------------------------- */

