/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* proximus.c								*/
/*									*/
/*  Copyright (C) 1998,1999 xavier@bru-noc.net				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  xavier@bru-noc.net							*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"
#include "comms/comms.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

static char ACK1[] = "connected\r\n";

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static struct proximus_env
{
	DRIVER_DEFAULT_ENV def;

} driver_env;


static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_comms_params", 	0, 1, NULL, 0,  "8N1",  0, 	 &(driver_env.def.comms_params)  	},
		{ RESOURCE_STRING,  "SMS_centre_number", 	0, 1, NULL, 0,  NULL,   0, 	 &(driver_env.def.centre_number)  	},
		{ RESOURCE_NUMERIC, "SMS_baud", 		0, 1, NULL, 0,  NULL,   9600, 	 &(driver_env.def.baud)  		},
		{ RESOURCE_NUMERIC, "SMS_deliver_timeout", 	0, 0, NULL, 0,  NULL,   30, 	 &(driver_env.def.deliver_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_timeout", 		0, 0, NULL, 0,  NULL,   10, 	 &(driver_env.def.timeout)  		},
		{ RESOURCE_NUMERIC, "SMS_write_timeout", 	0, 0, NULL, 0,  NULL,   10, 	 &(driver_env.def.write_timeout)  	},
		{ RESOURCE_NUMERIC, "SMS_max_deliver", 		0, 0, NULL, 0,  NULL,   0, 	 &(driver_env.def.max_deliver) 	        },
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,   0, 	 NULL  					}
	};

/* -------------------------------------------------------------------- */

#define DELIVERTIMEOUT 		(driver_env.def.deliver_timeout)
#define TIMEOUT 		(driver_env.def.timeout)
#define WRITETIMEOUT 		(driver_env.def.write_timeout)

/* -------------------------------------------------------------------- */

#define FD			(driver_env.def.fd)

/* -------------------------------------------------------------------- */

#define MAX_ENVELOPE 	64
#define MAX_NUMBER	20
#define MAX_MOBILENUM	20
#define MAX_MESSAGE	512
#define MAX_TAPMESSAGE	150
#define MAX_BUFSIZE	1024
#define EPROXIMUS_NORESPONSE -1

/* -------------------------------------------------------------------- */

static char buf[MAX_BUFSIZE +1];

/* -------------------------------------------------------------------- */

static int PROXIMUS_login(void);
static void PROXIMUS_buildmessage(char *, int, char *, char *);
static int  PROXIMUS_sendmessage(char *, char *);
static void PROXIMUS_hangup(void);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int PROXIMUS_login(void)
{
	char buf[MAX_RESPONSE_BUFSIZE];


	if (expstr(FD, buf, ACK1, MAX_RESPONSE_BUFSIZE, TIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Proximus Service Login\n");
	}
	else
	{	lprintf(LOG_STANDARD, "No Proximus Service Login\n");
		
		PROXIMUS_hangup();
		return EPROXIMUS_NORESPONSE;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void PROXIMUS_hangup(void)
{	default_hangup((DRIVER_DEFAULT_ENV *)(&driver_env));
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void PROXIMUS_buildmessage(char *proximus_message, int max_proximus_message_len, char *msisdn, char *message)
{
	int 	i, j, k;

	char buf       [512];
	char Originator[10];  /* Phone number of the originator, must be null */
	char TextDest  [256]; /* Test to be sent to the GSM */
	char TelDest   [20];  /* GSM number of the destination (3275695600) */
	char Len2String[16];
	char ChecksumString[512];
	int  TranNum;         /* Integer which identifiy the message sequence */
	char SendString[512]; /* String sent to the SMS-C */
	char TextConv  [512];

	/* ---------------------------- */

	proximus_message[0] = '\0';
        Originator [0] = '\0';
	TextConv   [0] = '\0';

	strcpy(TelDest, msisdn);
        strcpy(TextDest, message);

	/* Convert text */
	for (i=0; i < strlen(TextDest); i++)
	{
		sms_snprintf(buf, 512, "%02X", TextDest[i]);
		strcat(TextConv, buf);
	}

	sms_snprintf(SendString, 512, "/O/01/%s/%s/proximus/3/%s/", TelDest,
        	                                                    Originator,
                	                                            TextConv);

	/* Set length of message in a 5 digits string */
	sms_snprintf(Len2String, 16, "%05d", strlen(SendString) + 10);

	/* Set message sequence in a 2 digites string */
	TranNum = 1;

	sms_snprintf(ChecksumString, 512, "%02d/%s%s", TranNum, Len2String, SendString);

	/* Calculate checksum */
	j = 0;
	for (i=0; i < strlen(ChecksumString); i++)
	{
		k = ChecksumString[i];
		j += k;

		if (j >= 256)
		{	j -= 256;
		}
	}

	sms_snprintf(buf, 512, "%02X", j);
	
	/* Compile string to be send */
	sms_snprintf(proximus_message, max_proximus_message_len, "%c%s%s%c", 2, ChecksumString, buf, 3);
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static int PROXIMUS_sendmessage(char *msisdn, char *message)
{
	char 	proximus_message[MAX_MESSAGE + MAX_ENVELOPE];


	PROXIMUS_buildmessage(proximus_message, (MAX_MESSAGE + MAX_ENVELOPE), msisdn, message);
	
	twrite(FD, proximus_message, strlen(proximus_message), WRITETIMEOUT);

	if (expstr(FD, buf, "01/00019", MAX_BUFSIZE, DELIVERTIMEOUT) == 0)
	{
		lprintf(LOG_STANDARD, "Received Message Response\n"
		                      "SMSC Respsonse: %s\n", buf);
	}
	else
	{	lprintf(LOG_STANDARD, "No Message Response\n");

		PROXIMUS_hangup();
		return EPROXIMUS_NORESPONSE;
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
DEVICE_ENTRY proximus_device = {

	"PROXIMUS",
	"1.1",
	resource_list,
	(DRIVER_DEFAULT_ENV *)(&driver_env),

	default_init,
	default_main,
	default_validate_numeric_id,
	default_dial,
	default_hangup,
	default_send_disconnect,
	default_single_deliver,
	PROXIMUS_sendmessage,
	PROXIMUS_login
};

