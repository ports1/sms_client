/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* driver_comms.c							*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#if defined(MODEMLIB) && (MODEMLIB == LIBMODEM)
#include <termios.h>
#include <dial/modems.h>
#else
#include "comms/comms.h"
#endif

#include "common/common.h"
#include "logfile/logfile.h"
#include "driver.h"
#include "error.h"

#include "driver_comms.h"

/* -------------------------------------------------------------------- */

#if defined(MODEMLIB) && (MODEMLIB == LIBMODEM)
static void LIBMODEM_setup_comm_params(int fd, char *params);
static int LIBMODEM_dial(char *number, char *params, long baud);
static int num2baud (long baud_num);
#endif

/* -------------------------------------------------------------------- */
/* The following functions are used as wrappers to 			*/
/* The libmodem routine is you decide to link against it		*/
/* instead of the builtin smsmodem library.				*/
/* -------------------------------------------------------------------- */
#if defined(MODEMLIB) && (MODEMLIB == LIBMODEM)
static void LIBMODEM_setup_comm_params(int fd, char *params)
{
	struct 	termios 
		trm;

	
	if (params == NULL)
	{	return;
	}


	lprintf(LOG_VERBOSE, "Setting communications parameters: %s\n", params);

	if (strcmp(params, "8N1") == 0)
	{
		/* Default - No ACTION */
	}
	else if (strcmp(params, "7E1") == 0)
	{
		tcgetattr(fd, &trm);

		trm.c_cflag &= ~(CSIZE|CSTOPB|PARODD);
		trm.c_cflag |= CS7|PARENB;

		tcsetattr(fd, TCSAFLUSH, &trm);
	}
}        

static int num2baud(long baud) 
{
	switch (baud)
	{
		case 0:
			return B0;
		case 50:
			return B50;
		case 75:
			return B75;
		case 110:
			return B110;
		case 134:
			return B134;
		case 150:
			return B150;
		case 200:
			return B200;
		case 300:
			return B300;
		case 600:
			return B600;
		case 1200:
			return B1200;
		case 1800:
			return B1800;
		case 2400:
			return B2400;
		case 4800:
			return B4800;
		case 7200:
		case 9600:
			return B9600;
		case 12200:
		case 14400:
		case 16800:
		case 19200:
			return B19200;
		case 21600:
		case 24000:
		case 26400:
		case 28800:
		case 31200:
		case 33600:
		case 38400:
			return B38400;
	}

	return -1;
}

static int LIBMODEM_dial(char *number, char *params, long baud)
{
	int 	fd, mbaud;



	lprintf(LOG_VERBOSE, "Using Libmodem Package\n");
	

	mbaud = num2baud(baud);
	if (mbaud != -1)
	{	fd = bdial(number, num2baud(baud));
	}
	else
	{	lprintf(LOG_WARNING, "Unknown baud %ld\n", baud);
		return -1;
	}

	/* Libmodem-1.0.0 does not supports 	*/
	/* cofiguration of modem parameters.	*/
	/* Use own function to change them.	*/

	if (fd >= 0)
	{	LIBMODEM_setup_comm_params(fd, params);
	}

	return fd;
}

int SMS_dial(char *number, char *params, long baud)
{	return LIBMODEM_dial(number, params, baud);
}

void SMS_hangup(int fd)
{	hangup(fd);
}

#endif

