/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

static char *UCP_header(int tranRefNo, char ORC, int opType, int dataLen );
static char *UCP_checksum(char *header, char *data );

static char *UCP01_data(char *msisdn, char *message, int msgType);
static char *UCP51_data(char *msisdn, char *message, int msgType, char *ipaddress);
static char *UCP60_data(char *ipaddress, char *password );

static char *UCP_CallInputOp( char OCR, int opType, char *msgData );


char *UCP_CallInputOp01(char *msisdn, char *message );
char *UCP_CallInputOp51(char *msisdn, char *message, char *ipaddress);
char *UCP_CallInputOp60(char *ipaddress, char *password );

int UCP_parse_response(char *string);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *UCP_header(int tranRefNo, char ORC, int opType, int dataLen )
{
          int     msgLen;
   static char    msgHead[20];
                                                /*  HEADER section       */

   msgLen = 14 + dataLen + 2;          /* len(header+data+checksum) */

                            /* header:  TRN/LEN/ORC/OT/         */

   sprintf(msgHead, "%2.2d/%5.5d/%c/%2.2d/", tranRefNo, msgLen, ORC, opType);

   return msgHead;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *UCP_checksum(char *header, char *data )
{
   char   *p;
   int     sum;
   static char    buf[3];
                                                /*  CHECKSUM section       */
   sum = 0;
   for (p = header; *p != '\0'; p++)
   {
        sum += (unsigned char) *p;
        sum &= 0xFF;
   }
   for (p = data;   *p != '\0'; p++)
   {
        sum += (unsigned char) *p;
        sum &= 0xFF;
   }

  sprintf(buf, "%02X", sum);

  return buf;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *UCP01_data(char *msisdn, char *message, int msgType)
{
   static char    msgData[800];
          char   *src;
          char   *dest;
          int     nc;

                            /* data:  AdC/OAdc/OAC/MT/..   */
                            /*    ->  recipient///3/...    */

   nc = sprintf(msgData, "%s///%1.1d/", msisdn, msgType);

                            /* ... additional parameters depending on MT */
                            /* For MT=3 (alphanumeric) this is hex coded */
                            /* message data                              */

   dest = &msgData[nc];
   for (src = message;  *src;  src++)
   {
        sprintf(dest, "%02X", *src);
        dest += 2;
   }
   sprintf( dest, "/" );

   return msgData;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *UCP51_data(char *msisdn, char *message, int msgType, char *ipaddress)
{
   static char    msgData[800];
          char   *src;
          char   *dest;
          int     nc;


   nc = sprintf(msgData, "%s/%s/////////////////%1.1d//", msisdn, "originator", msgType );

   dest = &msgData[nc];
   for (src = message;  *src;  src++)
   {
        sprintf(dest, "%02X", *src);
        dest += 2;
   }

   sprintf(dest, "/////////////");

   return msgData;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *UCP60_data(char *ipaddress, char *password)
{
   static char    msgData[800];

   sprintf(msgData, "%s///1/%s//0100//////", ipaddress, password);
   return msgData;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *UCP_CallInputOp(char OCR, int opType, char *msgData)
{
   char   *msgHead,
          *msgCksm,
          ORC;

   static int    
          tranRefNo = 1;
   static char    buf[1024];



   msgHead = UCP_header( tranRefNo, ORC, opType, strlen(msgData) );

   msgCksm = UCP_checksum( msgHead, msgData );

              /* operational message:  <stx> header data checksum <etx> */

   sprintf(buf, "%c%s%s%s%c", S_STX, msgHead, msgData, msgCksm, S_ETX );

   tranRefNo++;
   return buf;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *UCP_CallInputOp01(char *msisdn, char *message)
{
   return UCP_CallInputOp( 'O', 01, UCP01_data( msisdn, message, 3 ) );
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *UCP_CallInputOp51(char *msisdn, char *message, char *ipaddress)
{
   return UCP_CallInputOp( 'O', 51, UCP51_data( msisdn, message, 3 , ipaddress) );
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *UCP_CallInputOp60(char *ipaddress, char *password)
{
   return UCP_CallInputOp( 'O', 60, UCP60_data( ipaddress, password ) );
}


/* -------------------------------------------------------------------- */
/* Return Values:                                                       */
/*       0 Positive ACK                                                 */
/*       1 Negative ACK                                                 */
/*      -1 Error                                                        */
/* -------------------------------------------------------------------- */
int UCP_parse_response(char *string)
{
        int     result;

        int     tranRefNo,
                length,
                opType;

        char    ORC, 
                ack;

        /* ------------------------------------------------------------ */
        /* Example:                                                     */
        /* <STX>01/00045/R/01/A/0041544180972:161298112313/A6<ETX>      */
        /*     trn len orc ot a        recip:time         chksum        */
        /* ------------------------------------------------------------ */

        result = sscanf(string, "\002%02d/%05d/%c/%02d/%c",
                        &tranRefNo,
                        &length,
                        &ORC,
                        &opType,
                        &ack        );


        if (result != 5 || ORC != 'R' || opType != 1 ) return -1;

        /* ---------------------------- */

        if     ( ack == 'A' ) result =  0;
        else if( ack == 'N' ) result =  1;
        else                  result = -1;

        return result;
}

