/* -------------------------------------------------------------------- */
/* Read a complete '\n' terminated string, discarding the '\n'		*/
/* upto maxlen characters will be read and a '\0' is appended		*/
/* -------------------------------------------------------------------- */
char *server_fgets(char *buf, int maxlen, int fd)
{
	int	result, 
		i;


	i = 0;
	buf[0] = '\0';
	while (i < (maxlen -1))
	{
		do
		{	result = read(fd, &buf[i], 1);

		} while ((result == -1) && (errno == EINTR));

		if (errno == -1)
		{	
			lprintf(LOG_ERROR, "read() failed\n");

			buf[i] = '\0';
			return NULL;
		}
		else
		if (result == 0)
		{
			if (i == 0)
			{
				buf[i] = '\0';
				return NULL;
			}

			break;
		}
		else
		{
			if (buf[i] == '\r')
			{
				continue;
			}
			else
			if (buf[i] == '\n')
			{
				break;
			}

			i++;
		}
	}
	
	buf[i] = '\0';
	return buf;
}

