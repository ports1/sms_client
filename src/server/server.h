/* -------------------------------------------------------------------- */
/* server.h								*/
/*									*/
/*									*/
/* Copyright (C) 1997 Angelo Masci					*/
/*       								*/
/* This program is free software; you can redistribute it and/or modify	*/
/* it under the terms of the GNU General Public License as published by	*/
/* the Free Software Foundation; either version 2 of the License, or	*/
/* (at your option) any later version.					*/
/*									*/
/* This program is distributed in the hope that it will be useful,	*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	*/
/* GNU General Public License for more details.				*/
/*									*/
/* You should have received a copy of the GNU General Public License	*/
/* along with this program; if not, write to the Free Software		*/
/* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		*/
/* 									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* --------------------------------------------------------------------
   $Id$
   -------------------------------------------------------------------- */

#define MAX_BACKLOG 	10     		/* Max connections to queue 	*/
#define MAX_STRING_LEN 	1024

/* -------------------------------------------------------------------- */

void sigchld_handler(int signum);
char *hgets(char *buf, int maxlen, int fd);

#if !defined(__GNUC__)
void hprintf(int fd, char *fmt, ...);
#else
void hprintf(int fd, char *fmt, ...)
             __attribute__ ((format (printf, 2, 3)));
#endif

int server_main(int server_port, void (*handle_child)(int new_fd), char *lockfile);
void default_echo(int new_fd);
int server_init(int server_port);
int deamon_init(void);
int get_client_information(int fd, char *host_name, char *ip_address);

/* -------------------------------------------------------------------- */



