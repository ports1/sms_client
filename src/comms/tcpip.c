/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* tcpip.c								*/
/*									*/
/*  Copyright (C) 1997,1998, 1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#if defined(OSF1) || defined(SOLARIS) && !defined(SOLARIS_2_4)
#include <strings.h>
#endif

#include "common/common.h"
#include "logfile/logfile.h"
#include "error.h"
#include "tcpip.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int TCPIP_connect(char *host, int port)
{
	int sockfd;
	struct hostent *he;
	struct sockaddr_in their_addr; 		/* connector's address information */


	if ((he=gethostbyname(host)) == NULL)	/* get the host info 		*/ 
	{	
		lprintf(LOG_ERROR, "gethostbyname() failed\n");
	  	return -1;		
	}

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{	
		lprintf(LOG_ERROR, "socket() failed\n");
		return -1;
	}

	their_addr.sin_family = AF_INET;	/* host byte order 		*/
	their_addr.sin_port = htons(port); 	/* short, network byte order 	*/

	their_addr.sin_addr = *((struct in_addr *)he->h_addr);

	memset(&(their_addr.sin_zero[0]), 0, 0);

	if (connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) 
	{	
		lprintf(LOG_ERROR, "connect() failed\n");
		return -1;
	}


	return sockfd;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int TCPIP_disconnect(int fd)
{	return close(fd);
}


