/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* modem_queue.c							*/
/*									*/
/*  Copyright (C) 2000 Angelo Masci					*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>

#if defined(SOLARIS)
#include <sys/mkdev.h>
#endif

#include "modem.h"
#include "logfile/logfile.h"
#include "common/common.h"
#include "expect.h"
#include "lock/lock.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

#if !defined(MMODEMDIR)
#error "MMODEMDIR undefined"
#else
#define MODEMDIR        MMODEMDIR
#endif


/* -------------------------------------------------------------------- */

#define MAX_BUFSIZE  1024

TOKEN_HEAP
	*modems_heap;

/* -------------------------------------------------------------------- */

static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_HEAP,     "modems", 			0, 1, NULL, 0,  NULL,       0,  		&modems_heap	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,       0, 	  NULL  		},
	};

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void MDM_queue_hangup(int modem)
{
	MDM_queue_hangup(modem);

	resource_unlock(lockfile);		/* Remove lock file	*/
						/* on this queue	*/
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

int modem_id = 0;

static char *get_first_modem_lockfile(void)
{
	return NULL;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char *get_next_modem_lockfile(void)
{
	return NULL;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int MDM_queue_dial(char *number, char data, char parity, char stop, long baud) 
{
	struct 	stat 
		stat_buf;

	int 	modem,
		res,
		ring_count;


	char 	fname[MAX_BUFSIZE],
		lockfile,
		first_lockfile;


	strcpy(fname, MODEMDIR);
	libcommon_strfcat(fname, "sms_modem_queue");

	if (read_resource_file(fname, resource_list, TRUE) != RESOURCE_FILE_OK)
	{	lprintf(LOG_ERROR, "MODEM QUEUE: Unrecoverable Failure Parsing modem queue file '%s'\n", fname); 
		exit(-1);
	}


	first_lockfile = get_first_lockfile();
	lockfile       = first_lockfile;

#if 0
	if (resource_test_lockdir(lockfile) == -1)
	{	return -1;
	}
#endif

	while (resource_lock(lockfile))
	{
		lockfile = get_next_lockfile();
		if (lockfile == NULL)
		{	lockfile = get_first_lockfile();
		}

		if (lockfile == first_lockfile)
		{
			resource_wait(lockfile, delay);
		}
	}

	return MDM_dial(number, data, parity, stop, baud);
}
