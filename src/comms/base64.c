/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* base64.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdlib.h>

#include "common/common.h"
#include "logfile/logfile.h"
#include "base64.h"
#include "error.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static char base64table[] = 
{
	'A', 'B', 'C', 'D', 
	'E', 'F', 'G', 'H', 
	'I', 'J', 'K', 'L', 
	'M', 'N', 'O', 'P', 
	'Q', 'R', 'S', 'T', 
	'U', 'V', 'W', 'X', 
	'Y', 'Z', 'a', 'b', 
	'c', 'd', 'e', 'f', 
	'g', 'h', 'i', 'j', 
	'k', 'l', 'm', 'n', 
	'o', 'p', 'q', 'r', 
	's', 't', 'u', 'v', 
	'w', 'x', 'y', 'z', 
	'0', '1', '2', '3', 
	'4', '5', '6', '7', 
	'8', '9', '+', '/'
};

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *encodeBase64(char *str)
{
	int 	data,
		count,
		value,
		i;
	char	*ptr,
		*eptr,
		*encoded_data;  



	encoded_data = (char *)malloc(sizeof(char) * (strlen(str) * 2));
	if (encoded_data == NULL)
	{	
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	data  = 0;
	count = 0;
	ptr   = str;
	eptr  = encoded_data;
	while(*ptr != 0)
	{
		count++;
		data |= (*ptr << ((3 - count) * 8));

		if (count == 3)
		{
			for(i=0; i<4; i++)
			{
				value = (data >> ((3 - i) * 6)) & 0x003F;

				*eptr++ = base64table[value];
			}

			data  = 0;
			count = 0;
		}

		ptr++;
	}

	if (count == 2)
	{
		/* = */


		for(i=0; i<3; i++)
		{
			value = (data >> ((3 - i) * 6)) & 0x003F;

			*eptr++ = base64table[value];
		}

		*eptr++ = '=';
	}


	if (count == 1)
	{	
		/* == */


		for(i=0; i<2; i++)
		{
			value = (data >> ((3 - i) * 6)) & 0x003F;

			*eptr++ = base64table[value];
		}

		*eptr++ = '=';
		*eptr++ = '=';
	}

	*eptr = '\0';


	return encoded_data;
}


