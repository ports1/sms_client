/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* test_queue.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>

#include "common/common.h"
#include "npipe.h"
#include "process.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	/* ---------------------------- */


	if (argc != 3)
	{
		fprintf(stderr, "Usage: %s <file> <queue>\n", argv[0]);
		exit(-1);
	}

	if (create_named_pipe(argv[1]) == -1)
	{
		fprintf(stderr, "Error: create_named_pipe() failed\n");
		exit(-1);
	}


	fprintf(stdout, "Waiting on named pipe '%s'...\n", argv[1]);

	if (block_on_named_pipe(argv[1]) == -1)
	{
		fprintf(stderr, "Error: block_on_named_pipe() failed\n");
		exit(-1);
	}

	fprintf(stdout, "Woken by write to '%s'\n", argv[1]);
	fprintf(stdout, "Processing queue '%s'...\n", argv[2]);

#if 0
	process_queue(argv[2]);
#endif

	fprintf(stdout, "Done\n");
	fprintf(stdout, "Deleting named pipe '%s'\n", argv[1]);


	if (delete_named_pipe(argv[1]) == -1)
	{
		fprintf(stderr, "Error: delete_named_pipe() failed\n");
		exit(-1);
	}

	return 0;
}              


