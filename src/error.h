/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* error.h								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */

#define UNDEFINED		-1

/* -------------------------------------------------------------------- */

#define EMESSAGETOOLONG		1
#define EUSAGE			2
#define ENOSERVICE		3
#define ENAMEEXPANSION		4
#define ESIGERR			5
#define EOPENLOG		6
#define EMALLOC			7
#define EDIAL			8

/* -------------------------------------------------------------------- */

#define EDELIVERY		10	/* One or more messages were	*/
					/* failed to be delivered	*/

/* -------------------------------------------------------------------- */

#define EVODAFONE_NORESPONSE	30
#define EVODAFONE_NONUMBER	31
#define EVODAFONE_NOMESSAGE	32
#define EVODAFONE_NODELIVERY	33

#define EORANGE_NORESPONSE	40
#define EORANGE_NOSERVICE	41
#define EORANGE_NONUMBER	42
#define EORANGE_NOMESSAGE	43
#define EORANGE_NODELIVERY	44
#define EORANGE_NODISCONNECT	45

#define EPAGEONE_NORESPONSE	50
#define EPAGEONE_NONUMBER	51
#define EPAGEONE_NOSERVICE	52
#define EPAGEONE_NOMESSAGE	53
#define EPAGEONE_NODELIVERY	54
#define EPAGEONE_ALPHA		55

#define ETAP_NOACK		60
#define ETAP_NOLOGIN		61
#define ETAP_NOREADY		62
#define ETAP_NOMESSAGE		63
#define ETAP_NODELIVERY		64
#define ETAP_NODISCONNECT 	65

#define EVODACOM_NORESPONSE	70
#define EVODACOM_NONUMBER	71
#define EVODACOM_NOMESSAGE	72

#define EMTN_NORESPONSE		80
#define EMTN_NONUMBER		81
#define EMTN_NODELIVERY		82

#define EONE2ONE_NORESPONSE	90
#define EONE2ONE_NONUMBER	91
#define EONE2ONE_NOSERVICE	92
#define EONE2ONE_NOMESSAGE	93
#define EONE2ONE_NODELIVERY	94
#define EONE2ONE_ALPHA		95

#define ELIBERTEL_NORESPONSE    100
#define ELIBERTEL_NONUMBER  	101
#define ELIBERTEL_NOMESSAGE     102
#define ELIBERTEL_NODELIVERY    103

#define ETIM_NORESPONSE         110
#define ETIM_NONUMBER           111
#define ETIM_NOSERVICE          112
#define ETIM_NOMESSAGE          113
#define ETIM_NODELIVERY         114
#define ETIM_ALPHA        	115
#define ETIM_NODISCONNECT 	116

#define EVODAPAGE_BLOCK_NOID       120
#define EVODAPAGE_BLOCK_NONUMBER   121
#define EVODAPAGE_BLOCK_NODELIVERY 122

#define ESNPP_NORESPONSE    	130
#define ESNPP_NONUMBER  	131
#define ESNPP_NOMESSAGE  	132
#define ESNPP_NODELIVERY  	133
#define ESNPP_NODISCONNECT   	134

#define EKPN_NOWELCOME 		140
#define EKPN_NORETURN 		141
#define EKPN_NOMENU1		142
#define EKPN_NOMENU2 		143
#define EKPN_NOMENU3 		144
#define EKPN_NONUMBER 		145
#define EKPN_NOMESSAGE 		146
#define EKPN_NOCONFIRM 		147
#define EKPN_NOSEND 		149

#define EANSWER_NONUMBER 	150
#define EANSWER_NOMESSAGE	151
#define EANSWER_NODELIVERY	152

#define EANSWER_NODISCONNECT	160
#define EANSWER_NOACK		161
#define EANSWER_NOLOGIN		162

#define EUCP_ACKFAILED		170
#define EUCP_BADACK		171
#define EUCP_NORESPONSE		172

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

