/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* sms_address.c							*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#if defined(LINUX)
#include <getopt.h>
#endif
#if defined(NEXT)
#include <libc.h>
#endif

#include "error.h"
#include "sms_list.h"
#include "expand.h"
#include "common/common.h"
#include "logfile/logfile.h"
#include "version.h"
#include "resource/resource.h"

/* -------------------------------------------------------------------- */

#if !defined(MVERSION)
#error "MVERSION undefined"
#else
#define VERSION MVERSION
#endif

#if !defined(MLOGFILE)
#error "MLOGFILE undefined"
#else
#define LOGFILE         MLOGFILE
#endif

#if !defined(MLOGLEVEL)
#error "MLOGLEVEL undefined" 
#else
#define LOGLEVEL	MLOGLEVEL
#endif

#if !defined(MSERVICEDIR)
#error "MSERVICEDIR undefined"
#else
#define SERVICEDIR      MSERVICEDIR
#endif

#define CONFIG_FILE  (MSERVICEDIR "/sms_config")
 
/* -------------------------------------------------------------------- */

static 	char 
	*SMS_default_service;


static 	RESOURCE resource_list[] = 
	{
		{ RESOURCE_STRING,  "SMS_default_service", 	0, 1, NULL, 0,       "CELLNET",    0,  &SMS_default_service	},
		{ RESOURCE_NULL,     NULL, 			0, 1, NULL, 0,  NULL,              0,  NULL  			}
	};

/* -------------------------------------------------------------------- */

void usage(char *file);
int main(int, char *[]);

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void usage(char *file)
{
	printf("Usage: %s [-v]\n", file);
	printf("       %s [-l loglevel] [-L] [-d delimeter]  name[,name]\n", file);
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
	char	*ptr;
	int	c,
		long_flag,
		delimeter;

	SMS_list
		*node,
		*numbers;

	TOKEN_HEAP
		*global,
		*local;
		

	/* ---------------------------- */

	set_logfile(LOGFILE);
	set_loglevel(LOGLEVEL);
	set_consolelog(TRUE);

	/* ---------------------------- */

	delimeter = ':';
	long_flag = FALSE;

	while ((c = getopt (argc, argv, "vl:d:L")) != -1)
        {
                switch (c)
                {	case 'v':
				printf("%s %s\n", argv[0], VERSION);
                                exit(0);

                        case 'l':  
				set_loglevel((int)strtol(optarg, &ptr, 10));
				if (ptr == optarg)
				{
					lprintf(LOG_ERROR, "Option l requires an argument\n");
	     	                        usage(argv[0]);
                	                exit(EUSAGE);
				}
                                
                                break;

                	case 'L':
				long_flag = TRUE;
				break;

                	case 'd':
				if (strlen(optarg) > 1)
                                {	fprintf(stderr, "Invalid delimeter `%s'\n", optarg);
	                                usage(argv[0]);
        	                        exit(-1);
				}

				delimeter = optarg[0];
				break;

                        case '?':
#if !defined(NEXT)
                                fprintf(stderr, "Unknown option `-%c'\n", optopt);
#endif
                                usage(argv[0]);
                                exit(-1);

                        default:
                                abort ();
                }
        }


	/* ---------------------------- */

	if ((argc - optind) < 1)
	{
		usage(argv[0]);
		exit(EUSAGE);
	}

	/* ---------------------------- */
	/* Get Default SERVICE 		*/


	if (read_resource_file(CONFIG_FILE, resource_list, TRUE) != RESOURCE_FILE_OK)
	{	lprintf(LOG_ERROR, "Unrecoverable Failure Parsing file '%s'\n", CONFIG_FILE);
		exit(1);
	}

	/* ---------------------------- */
	/* Get and expand NAMES|NUMBERS	*/
	
	if (SMS_dual_openrc(&global, &local) == -1)
	{	exit(-1);
	}

	numbers = SMS_expandnumber(global, local, "<NULL>", argv[optind], SMS_default_service);
	SMS_dual_closerc(global, local);


	/* ---------------------------- */
	/* Check NAMES|NUMBERS		*/

	if (SMS_validate_expanded_numbers(numbers))
	{
		fprintf(stderr, "ERROR: Expanding names\n");
		exit(-1);
	}

	/* ---------------------------- */

	node = get_first(numbers);
	while (node != NULL)
	{
		printf("%s%c%s%c%s\n", node->name, 
		                       delimeter, 
		                       node->service, 
		                       delimeter,
		                       node->number);

		node = get_next(node);
	}

	free_list(numbers);
	return 0;
}


