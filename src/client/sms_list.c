/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* sms_list.c								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <unistd.h>

#include "sms_list.h"
#include "error.h"
#include "logfile/logfile.h"
#include "common/common.h"

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
#if 0
SMS_list *insert_list(SMS_list *main_list, SMS_list *list)
{
	SMS_list
		*node;


	if (list == NULL)
	{	return main_list;
	}

	if (main_list == NULL)
	{	return list;
	}

	node = list;
	while(node->next != NULL)
	{	node = node->next;
	}

	node->next = main_list->next;
	main_list->next = list;

	return main_list;
}
#else
SMS_list *insert_list(SMS_list *main_list, SMS_list *list)
{
	SMS_list
		*node;


	if (list == NULL)
	{	return main_list;
	}

	if (main_list == NULL)
	{	return list;
	}

	node = list;
	while(node->next != NULL)
	{	node = node->next;
	}

	node->next = main_list;
	return list;
}
#endif

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_parent_list *find_list(SMS_parent_list *list, char *service)
{
	SMS_parent_list
		*node;


	node = get_first_parent(list);
	while (node != NULL)
	{
		if (strcmp(node->child->service, service) == 0)
		{
			return node;
		}

		node = get_next_parent(node);
	}

	return NULL;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
static void free_node(SMS_list *node)
{
	free(node->name);
	free(node->service);
	free(node->number);
	free(node);
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_list *add_item(SMS_list *list, char *name, char *service, char *number)
{
	SMS_list
		*node;


	node = (SMS_list *)malloc(sizeof(SMS_list));
	if (node == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	node->name = (char *)malloc(sizeof(char) * (strlen(name) +1));
	if (node->name == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	strcpy(node->name, name);

	node->service = (char *)malloc(sizeof(char) * (strlen(service) +1));
	if (node->service == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	strcpy(node->service, service);

	node->number = (char *)malloc(sizeof(char) * (strlen(number) +1));
	if (node->number == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	strcpy(node->number, number);

	node->delivery = -1;

	if (list == NULL)
	{
		node->next = NULL;
		return node;
	}

	node->next = list->next;
	list->next = node;

	return list;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_list *dupnode(SMS_list *node)
{
	SMS_list
		*new_node;


	new_node = add_item(NULL, node->name, node->service, node->number);
	new_node->delivery = node->delivery;

	return new_node;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_list *get_first(SMS_list *node)
{
	return node;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_list *get_next(SMS_list *node)
{
	if (node == NULL)
	{	return NULL;
	}

	return node->next;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_parent_list *add_item_parent(SMS_parent_list *list, SMS_list *child)
{
	SMS_parent_list
		*node;


	node = (SMS_parent_list *)malloc(sizeof(SMS_parent_list));
	if (node == NULL)
	{
		lprintf(LOG_ERROR, "malloc() failed\n");
		exit(EMALLOC);
	}

	node->child = child;

	if (list == NULL)
	{
		node->next = NULL;
		return node;
	}

	node->next = list->next;
	list->next = node;

	return list;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_parent_list *get_first_parent(SMS_parent_list *node)
{
	return node;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_parent_list *get_next_parent(SMS_parent_list *node)
{
	if (node == NULL)
	{	return NULL;
	}

	return node->next;
}

/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_list *get_child(SMS_parent_list *node)
{
	if (node == NULL)
	{	return NULL;
	}

	return node->child;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *get_number(SMS_list *node)
{
	if (node == NULL)
	{	return NULL;
	}

	return node->number;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *get_name(SMS_list *node)
{
	if (node == NULL)
	{	return NULL;
	}

	return node->name;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
char *get_service(SMS_list *node)
{
	if (node == NULL)
	{	return NULL;
	}

	return node->service;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void set_delivery(SMS_list *node, int delivery)
{
	if (node == NULL)
	{	return;
	}

	node->delivery = delivery;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
int get_delivery(SMS_list *node)
{
	if (node == NULL)
	{	return 0;
	}

	return node->delivery;
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
void free_list(SMS_list *list)
{
	SMS_list
		*node,
		*next;


	node = list;
	while (node != NULL)
	{
		next = node->next;
		free_node(node);
		node = next;
	}
}


/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
SMS_list *find_number(SMS_list *list, char *str)
{
	SMS_list
		*node;

	
	node = get_first(list);
	while (node != NULL)
	{
		if (strcmp(get_number(node), str) == 0)
		{	return node;
		}

		node = get_next(node);
	}

	return NULL;
}


/* -------------------------------------------------------------------- */
/* Gather all items that contain identical				*/
/* services and generates a lists of lists containing			*/
/* identical items							*/
/* Example:								*/
/*									*/
/*		|-- Type A						*/
/*		|-- Type C						*/
/*		|-- Type B						*/
/*		|-- Type C						*/
/*		|-- Type A						*/
/*		|-- Type A						*/
/*									*/
/* Becomes:								*/
/*									*/
/*		|							*/
/*		|-------|-- Type A					*/
/*		|       |-- Type A					*/
/*		|       |-- Type A					*/
/*		|							*/
/*		|-------|-- Type B					*/
/*		|							*/
/*		|-------|-- Type C					*/
/*		        |-- Type C					*/
/*									*/
/* -------------------------------------------------------------------- */
SMS_parent_list *gather(SMS_list *list)
{
	SMS_parent_list 
		*parent_list,
		*found_list;

	SMS_list 
		*node,
		*new_node;


	parent_list = NULL;
	node = get_first(list);
	while (node != NULL)
	{
		new_node   = dupnode(node);
		found_list = find_list(parent_list, get_service(node)); 

		if (found_list == NULL)
		{
			found_list = add_item_parent(NULL, new_node);
			found_list->next = parent_list;
			parent_list = found_list;
		}
		else
		{	list = insert_list(found_list->child, new_node);
			found_list->child = list;
		}

		node = get_next(node);
	}

	return parent_list;
}


