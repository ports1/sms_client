/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* sms_list.h								*/
/*									*/
/*  Copyright (C) 1997,1998 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#ifndef _SMSLIST_H
#define _SMSLIST_H

/* -------------------------------------------------------------------- */

struct SMS_list_struct
{
	struct SMS_list_struct 
		*next;

	char 	*name,
		*service,
		*number;

	int 	delivery;
};

typedef struct SMS_list_struct SMS_list;

/* -------------------------------------------------------------------- */

struct SMS_parent_list_struct
{
	struct SMS_parent_list_struct 
		*next;

	struct SMS_list_struct
		*child;
};

typedef struct SMS_parent_list_struct SMS_parent_list;

/* -------------------------------------------------------------------- */

SMS_list *insert_list(SMS_list *main_list, SMS_list *list);
SMS_list *dupnode(SMS_list *node);
SMS_list *get_first(SMS_list *node);
SMS_list *get_next(SMS_list *node);

char *get_number(SMS_list *node);
char *get_name(SMS_list *node);
char *get_service(SMS_list *node);

void set_delivery(SMS_list *node, int delivery);

int get_delivery(SMS_list *node);

SMS_parent_list *add_item_parent(SMS_parent_list *list, SMS_list *child);
SMS_parent_list *get_first_parent(SMS_parent_list *node);
SMS_parent_list *get_next_parent(SMS_parent_list *node);
SMS_list *get_child(SMS_parent_list *node);
SMS_parent_list *find_list(SMS_parent_list *list, char *service);

void free_list(SMS_list *list);

SMS_list *find_number(SMS_list *list, char *str);
SMS_list *dupnode(SMS_list *node);
SMS_list *add_item(SMS_list *list, char *name, char *service, char *number);

SMS_parent_list *gather(SMS_list *list);


#endif
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */


