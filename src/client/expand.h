/* -------------------------------------------------------------------- */
/* SMS Client, send messages to mobile phones and pagers		*/
/*									*/
/* expand.h								*/
/*									*/
/*  Copyright (C) 1997,1998,1999 Angelo Masci				*/
/*									*/
/*  This library is free software; you can redistribute it and/or	*/
/*  modify it under the terms of the GNU Library General Public		*/
/*  License as published by the Free Software Foundation; either	*/
/*  version 2 of the License, or (at your option) any later version.	*/
/*									*/
/*  This library is distributed in the hope that it will be useful,	*/
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	*/
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*/
/*  Library General Public License for more details.			*/
/*									*/
/*  You should have received a copy of the GNU Library General Public	*/
/*  License along with this library; if not, write to the Free		*/
/*  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.	*/
/*									*/
/*  You can contact the author at this e-mail address:			*/
/*									*/
/*  angelo@styx.demon.co.uk						*/
/*									*/
/* -------------------------------------------------------------------- */
/* $Id$
   -------------------------------------------------------------------- */

#ifndef _EXPAND_H
#define _EXPAND_H

#include "sms_list.h"
#include "parser/gs_token.h"

/* -------------------------------------------------------------------- */

int SMS_dual_openrc(TOKEN_HEAP **global, TOKEN_HEAP **local);
void SMS_dual_closerc(TOKEN_HEAP *global, TOKEN_HEAP *local);

SMS_list *SMS_expandnumber(TOKEN_HEAP *global, TOKEN_HEAP *local, char *id, char *number, char *default_service);

int SMS_validate_expanded_numbers(SMS_list *numbers);

#endif
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */

